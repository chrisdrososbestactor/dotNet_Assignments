﻿using Newtonsoft.Json;
using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Printing.ConsoleApp;

namespace Printing.Services.Tests
{
    [TestClass]
    public class PrintingTest
    {
        [TestMethod]
        public void PrintHelloWorldTest()
        {
            var output = Printing.PrintHelloWorld();

            Assert.AreEqual(output, "Hello World!");
        }
        [TestMethod]
        public void PrintOrderTest()
        {
            var order = new Order();
            var printing = new Printing();
            var json = JsonConvert.SerializeObject(order);

            Assert.AreEqual(json, printing.PrintOrder(order));
        }
    }
}

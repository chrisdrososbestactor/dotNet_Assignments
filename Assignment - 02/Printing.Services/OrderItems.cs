﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Printing.Services
{
    public class Order
    {
        //A List with the IDs of the Order Items
        public List<int> OrderItems { get; set; }       
        public string ClientName { get; set; }
        public DateTime DateOrdered { get; set; }
        public string EmployeeName { get; set; }
        public int Price { get; set; }

    }
}

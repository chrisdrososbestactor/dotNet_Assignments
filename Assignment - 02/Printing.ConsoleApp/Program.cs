﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Printing.Services;

namespace Printing.ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            var printHelper = new PrintHelper();
            printHelper.PrintProcedure();
        }
    }
}

use AdventureWorks2017;
select round(min(TotalDue),2) as minimum,round(max(TotalDue),0) as maximum, st.Name
from Sales.SalesOrderHeader as h
inner join Sales.SalesOrderHeaderSalesReason as sohsr
on h.SalesOrderID = sohsr.SalesOrderID
inner join  Sales.SalesTerritory as st
on st.TerritoryID = h.TerritoryID
inner join sales.SalesReason as sr
on sr.SalesReasonID = sohsr.SalesReasonID
where sr.ReasonType='Marketing'
group by st.Name
order by st.Name;
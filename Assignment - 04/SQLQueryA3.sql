use AdventureWorks2017;
select FirstName,MiddleName,LastName,EmailAddress
from Person.Person p
inner join Person.EmailAddress as ea

on p.BusinessEntityID = ea.EmailAddressID
where PersonType = 'EM'
and LastName like 'L%'
Order by firstname; 
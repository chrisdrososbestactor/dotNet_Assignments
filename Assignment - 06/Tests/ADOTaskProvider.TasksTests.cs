﻿using System.Collections.Generic;
using System.Linq;
using CoreReportService;
using CoreReportService.Models;
using DAL_ADO;
using DBModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Services.Common.Interfaces;

namespace Tests
{
    [TestClass]
    public class ADODataReportsTests
    {
        string ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017";
        string DalModeSelected = "ADO";
        public ADODataReportsTests() { }
        [TestMethod]
        public void AvgSickleaveHoursPerEmployeeMaritalStatusTest()
        {
            // Arrange
            // Mock Database Creation
            var mockDatabase = new Mock<ADOTaskProvider.GetDataProvider>(ConnectionString).As<IGetAllData>();
            mockDatabase.CallBase = true;
            mockDatabase.Setup(x => x.GetAllEmployees()).Returns(new MockDatabaseProvider().GetAllMockEmployees);
            mockDatabase.Setup(x => x.GetAllPersonEmailAdresses()).Returns(new MockDatabaseProvider().GetAllMockPersonEmailAddresses);
            mockDatabase.Setup(x => x.GetAllPersons()).Returns(new MockDatabaseProvider().GetAllMockPersons);
            mockDatabase.Setup(x => x.GetAllSaleOrderHeaderSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaderSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesOrderHeaders()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaders);
            mockDatabase.Setup(x => x.GetAllSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesTerritories()).Returns(new MockDatabaseProvider().GetAllMockSalesTerritories);

            // Mock ADOTaskProvider.GetDataProvider
            var mockTaskProvider = new ADOTaskProvider.Tasks(mockDatabase.Object);
            
            // Mock factory configured to return the desired provider
            var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                temp.GetTaskProvider() == mockTaskProvider // Return the actual provider for testing
            );

            // CoreReportService.DataReports with FakeDatabase
            var dataReports = new DataReports(mockFactory);

            // Act
            var result = dataReports.AvgSickleaveHoursPerEmployeeMaritalStatus().ToList();

            // Assert
            Assert.IsTrue(result.Count == 1);
        }
        [TestMethod]
        public void EmployeeListThatLastNameStartsWithLTest()
        {
            // Arrange
            // Mock Database Creation
            var mockDatabase = new Mock<ADOTaskProvider.GetDataProvider>(ConnectionString).As<IGetAllData>();
            mockDatabase.CallBase = true;
            mockDatabase.Setup(x => x.GetAllEmployees()).Returns(new MockDatabaseProvider().GetAllMockEmployees);
            mockDatabase.Setup(x => x.GetAllPersonEmailAdresses()).Returns(new MockDatabaseProvider().GetAllMockPersonEmailAddresses);
            mockDatabase.Setup(x => x.GetAllPersons()).Returns(new MockDatabaseProvider().GetAllMockPersons);
            mockDatabase.Setup(x => x.GetAllSaleOrderHeaderSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaderSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesOrderHeaders()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaders);
            mockDatabase.Setup(x => x.GetAllSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesTerritories()).Returns(new MockDatabaseProvider().GetAllMockSalesTerritories);

            // Mock ADOTaskProvider.GetDataProvider
            var mockTaskProvider = new ADOTaskProvider.Tasks(mockDatabase.Object);

            // Mock factory configured to return the desired provider
            var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                temp.GetTaskProvider() == mockTaskProvider // Return the actual provider for testing
            );

            // CoreReportService.DataReports with FakeDatabase
            var dataReports = new DataReports(mockFactory);

            // Act
            var result = dataReports.EmployeeListThatLastNameStartsWithL().ToList();

            // Assert
            Assert.IsTrue(result.Count == 1);
        }
        [TestMethod]
        public void GetNumEmployeesByGenderTest()
        {
            // Arrange
            // Mock Database Creation
            var mockDatabase = new Mock<ADOTaskProvider.GetDataProvider>(ConnectionString).As<IGetAllData>();
            mockDatabase.CallBase = true;
            mockDatabase.Setup(x => x.GetAllEmployees()).Returns(new MockDatabaseProvider().GetAllMockEmployees);
            mockDatabase.Setup(x => x.GetAllPersonEmailAdresses()).Returns(new MockDatabaseProvider().GetAllMockPersonEmailAddresses);
            mockDatabase.Setup(x => x.GetAllPersons()).Returns(new MockDatabaseProvider().GetAllMockPersons);
            mockDatabase.Setup(x => x.GetAllSaleOrderHeaderSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaderSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesOrderHeaders()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaders);
            mockDatabase.Setup(x => x.GetAllSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesTerritories()).Returns(new MockDatabaseProvider().GetAllMockSalesTerritories);

            // Mock ADOTaskProvider.GetDataProvider
            var mockTaskProvider = new ADOTaskProvider.Tasks(mockDatabase.Object);

            // Mock factory configured to return the desired provider
            var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                temp.GetTaskProvider() == mockTaskProvider // Return the actual provider for testing
            );

            // CoreReportService.DataReports with FakeDatabase
            var dataReports = new DataReports(mockFactory);

            // Act
            var result = dataReports.GetNumEmployeesByGender().ToList();

            // Assert
            Assert.IsTrue(result.Count == 1);
        }
        [TestMethod]
        public void SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersTest()
        {
            // Arrange
            // Mock Database Creation
            var mockDatabase = new Mock<ADOTaskProvider.GetDataProvider>(ConnectionString).As<IGetAllData>();
            mockDatabase.CallBase = true;
            mockDatabase.Setup(x => x.GetAllEmployees()).Returns(new MockDatabaseProvider().GetAllMockEmployees);
            mockDatabase.Setup(x => x.GetAllPersonEmailAdresses()).Returns(new MockDatabaseProvider().GetAllMockPersonEmailAddresses);
            mockDatabase.Setup(x => x.GetAllPersons()).Returns(new MockDatabaseProvider().GetAllMockPersons);
            mockDatabase.Setup(x => x.GetAllSaleOrderHeaderSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaderSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesOrderHeaders()).Returns(new MockDatabaseProvider().GetAllMockSalesOrderHeaders);
            mockDatabase.Setup(x => x.GetAllSalesReasons()).Returns(new MockDatabaseProvider().GetAllMockSalesReasons);
            mockDatabase.Setup(x => x.GetAllSalesTerritories()).Returns(new MockDatabaseProvider().GetAllMockSalesTerritories);

            // Mock ADOTaskProvider.GetDataProvider
            var mockTaskProvider = new ADOTaskProvider.Tasks(mockDatabase.Object);

            // Mock factory configured to return the desired provider
            var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                temp.GetTaskProvider() == mockTaskProvider // Return the actual provider for testing
            );

            // CoreReportService.DataReports with FakeDatabase
            var dataReports = new DataReports(mockFactory);

            // Act
            var result = dataReports.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders().ToList();

            // Assert
            Assert.IsTrue(result.Count == 1);
        }
    }
}

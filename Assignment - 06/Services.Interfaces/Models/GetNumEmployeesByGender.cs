﻿using System;

namespace Services.Common.Models
{
    /// <summary>
    /// A list of employees we have broken down by gender (male, female)
    /// </summary>
    public class GetNumEmployeesByGender
    {
        public string Gender { get; set; }
        public int Count { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using Services.Common.Models;

namespace Services.Common.Interfaces
{
    public interface ITaskEnumerableProvider
    {
        IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

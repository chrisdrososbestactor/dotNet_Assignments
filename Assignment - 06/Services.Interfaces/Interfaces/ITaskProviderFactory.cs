﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Common.Interfaces
{
    public interface ITaskProviderFactory
    {
        ITaskEnumerableProvider GetTaskProvider();
    }
}

﻿using Services.Common.Models;
using System.Collections.Generic;
using System.Text;

namespace Services.Common.Interfaces
{
    public interface IDataReports
    {
        IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

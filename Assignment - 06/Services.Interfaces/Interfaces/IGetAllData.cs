﻿using System;
using System.Collections.Generic;
using System.Text;
using DBModels;

namespace Services.Common.Interfaces
{
    public interface IGetAllData
    {
        IEnumerable<Employee> GetAllEmployees();
        IEnumerable<Person> GetAllPersons();
        IEnumerable<EmailAddress1> GetAllPersonEmailAdresses();
        IEnumerable<SalesOrderHeader> GetAllSalesOrderHeaders();
        IEnumerable<SalesOrderHeaderSalesReason> GetAllSaleOrderHeaderSalesReasons();
        IEnumerable<SalesReason> GetAllSalesReasons();
        IEnumerable<SalesTerritory> GetAllSalesTerritories();
    }
}

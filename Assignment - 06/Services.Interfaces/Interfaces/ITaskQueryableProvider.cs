﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Services.Common.Models;

namespace Services.Common.Interfaces
{
    public interface ITaskQueryableProvider
    {
        IQueryable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IQueryable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IQueryable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IQueryable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

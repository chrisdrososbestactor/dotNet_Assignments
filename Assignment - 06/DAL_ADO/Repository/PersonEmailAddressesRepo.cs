﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class PersonEmailAddressesRepo : IDataReader<EmailAddress1>
    {
        public IEnumerable<EmailAddress1> GetData(string connectionString)
        {
            var queries = new PersonEmailAddressQueries();
            var personEmailAddressList = new List<EmailAddress1>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {                
                using (SqlCommand command = new SqlCommand(queries.GetAllFromPersonEmailAddressQuery(), connection))
                {
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var personEmailAddress = new EmailAddress1()
                            {
                                BusinessEntityId = reader.GetInt32(0),
                                EmailAddressId = reader.GetInt32(1),
                                EmailAddress = reader.GetString(2),
                                Rowguid = reader.GetGuid(3),
                                ModifiedDate = reader.GetDateTime(4)
                            };
                            personEmailAddressList.Add(personEmailAddress);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return personEmailAddressList;
        }
    }
}

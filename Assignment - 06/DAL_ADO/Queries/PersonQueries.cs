﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class PersonQueries
    {
        public string GetAllFromPersonQuery()
        {
            return @"SELECT 
                     [BusinessEntityID] 
                    ,[PersonType] 
                    ,[NameStyle] 
                    ,[Title] 
                    ,[FirstName] 
                    ,[MiddleName] 
                    ,[LastName] 
                    ,[Suffix] 
                    ,[EmailPromotion] 
                    ,[AdditionalContactInfo] 
                    ,[Demographics] 
                    ,[rowguid] 
                    ,[ModifiedDate] 
                    FROM [Person].[Person]";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class SalesOrderHeaderQueries
    {
        public string GetAllFromSalesOrderHeaderQuery()
        {
            return @"SELECT [SalesOrderID] 
                  ,[RevisionNumber] 
                  ,[OrderDate] 
                  ,[DueDate] 
                  ,[ShipDate] 
                  ,[Status] 
                  ,[OnlineOrderFlag] 
                  ,[SalesOrderNumber] 
                  ,[PurchaseOrderNumber] 
                  ,[AccountNumber] 
                  ,[CustomerID] 
                  ,[SalesPersonID] 
                  ,[TerritoryID] 
                  ,[BillToAddressID] 
                  ,[ShipToAddressID] 
                  ,[ShipMethodID] 
                  ,[CreditCardID] 
                  ,[CreditCardApprovalCode] 
                  ,[CurrencyRateID] 
                  ,[SubTotal] 
                  ,[TaxAmt] 
                  ,[Freight] 
                  ,[TotalDue] 
                  ,[Comment] 
                  ,[rowguid] 
                  ,[ModifiedDate] 
              FROM [Sales].[SalesOrderHeader]";
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class SalesOrderHeaderSalesReasonQueries
    {
        public string GetAllFromSalesOrderHeaderSalesReasonQuery()
        {
           return @"SELECT 
                   [SalesOrderID]
                  ,[SalesReasonID]
                  ,[ModifiedDate]
              FROM [Sales].[SalesOrderHeaderSalesReason]";
        }        
    }
}

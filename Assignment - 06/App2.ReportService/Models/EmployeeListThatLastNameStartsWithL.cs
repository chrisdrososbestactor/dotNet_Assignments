﻿using System;

namespace App2.ReportService.Models
{
    public class EmployeeListThatLastNameStartsWithL
    {
        public string FirtName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PersonType { get; set; }
    }
}

﻿using App2.ReportService.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces
{
    public interface IPrintable
    {
        string PrintHelloWorld();
        string PrintOrder(Order order);
        
    }
}

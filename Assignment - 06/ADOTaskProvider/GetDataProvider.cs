﻿using System;
using DAL_ADO;
using System.Linq;
using Services.Common.Models;
using System.Collections.Generic;
using Services.Common.Interfaces;
using DBModels;

namespace ADOTaskProvider
{
    public class GetDataProvider : IGetAllData
    {
        protected string ConnectionString;
        public GetDataProvider(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public IEnumerable<Employee> GetAllEmployees()
        {
            return new EmployeeRepo().GetData(ConnectionString);
        }
        public IEnumerable<Person> GetAllPersons()
        {
            return new PersonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<EmailAddress1> GetAllPersonEmailAdresses()
        {
            return new PersonEmailAddressesRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesOrderHeader> GetAllSalesOrderHeaders()
        {
            return new SalesOrderHeaderRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesOrderHeaderSalesReason> GetAllSaleOrderHeaderSalesReasons()
        {
            return new SalesOrderHeaderSalesReasonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesReason> GetAllSalesReasons()
        {
            return new SalesReasonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesTerritory> GetAllSalesTerritories()
        {
            return new SalesTerritoryRepo().GetData(ConnectionString);
        }
    }
}

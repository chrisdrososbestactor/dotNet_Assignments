﻿using System;
using System.Collections.Generic;
using System.Linq;
using Services.Common.Models;
using DAL_EFCore;
using Services.Common.Interfaces;

namespace CoreReportService
{
    public class DataReports : IDataReports
    {
        protected ITaskEnumerableProvider TaskProvider;
        static protected string ConnectionString;
        static protected string DalModeSelected;
        AdventureWorks2017Context Context;
        public DataReports(EFCoreTaskProvider.Tasks taskProvider)
        {
            TaskProvider = taskProvider;
        }
        public DataReports(string connectionString, string dalModeSelected)
        {
            ConnectionString = connectionString;
            DalModeSelected = dalModeSelected;           
        }
        void ConfigureDAL()
        {
            switch (DalModeSelected)
            {
                case "ADO":
                    TaskProvider = new ADOTaskProvider.Tasks(ConnectionString);
                    break;
                case "EFCore":
                    Context = new AdventureWorks2017Context(ConnectionString);
                    TaskProvider = new EFCoreTaskProvider.Tasks(Context);
                    break;
            }
        }
        void CloseConnections()
        {
            if (DalModeSelected == "EFCore")
                if (Context != null)
                    Context.Dispose();
        }        
        public IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
        {
            ConfigureDAL();
            var result = TaskProvider.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders().ToList();
            CloseConnections();
            return result;
        }
        public IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus()
        {
            ConfigureDAL();
            var result = TaskProvider.AvgSickleaveHoursPerEmployeeMaritalStatus().ToList();
            CloseConnections();
            return result;
        }
        public IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL()
        {
            ConfigureDAL();
            var result = TaskProvider.EmployeeListThatLastNameStartsWithL().ToList();
            CloseConnections();
            return result;
        }
        public IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender()
        {
            ConfigureDAL();
            var result = TaskProvider.GetNumEmployeesByGender().ToList();
            CloseConnections();
            return result;
        }
    }
    
}

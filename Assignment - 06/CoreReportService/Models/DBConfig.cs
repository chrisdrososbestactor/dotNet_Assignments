﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CoreReportService.Models
{
    public class DBConfig
    {
        // All the configuration parameter for the connection with the server goes here
        public string DAL { get; set; } = String.Empty;
        public string ConnectionString { get; set; } = String.Empty;

    }
}

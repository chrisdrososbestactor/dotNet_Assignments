﻿using System;
using System.Collections.Generic;
using System.Linq;
using Services.Common.Models;
using Services.Common.Interfaces;
using CoreReportService.Models;
using ADOTaskProvider;

namespace CoreReportService
{
    public class DefaultTaskProviderFactory : ITaskProviderFactory
    {
        readonly DBConfig Config;

        public DefaultTaskProviderFactory(DBConfig config)
        {
            Config = config;
        }

        public ITaskEnumerableProvider GetTaskProvider()
        {
            switch (Config.DAL)
            {
                case "ADO":
                    return new ADOTaskProvider.Tasks(Config.ConnectionString);
                case "EFCore":
                    return new EFCoreTaskProvider.Tasks(Config.ConnectionString);
                default:
                    throw new FormatException("The format of the variable which represent the selected DAL was not correct");
            }
        }
    }
}

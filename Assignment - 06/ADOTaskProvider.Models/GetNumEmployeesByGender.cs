﻿using System;

namespace ADOTaskProvider.Models
{
    public class GetNumEmployeesByGender
    {
        public string Gender { get; set; }
        public int Count { get; set; }
    }
}

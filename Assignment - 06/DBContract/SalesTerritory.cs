﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModels.ADO
{
    public class SalesTerritory
    {
        public int TerritoryID { get; set; }
        public string Name { get; set; } = String.Empty;
        public string CountryRegionCode { get; set; } = String.Empty;
        public string Group { get; set; } = String.Empty;
        public decimal SalesYTD { get; set; }
        public decimal SalesLastYear { get; set; }
        public decimal CostYTD { get; set; }
        public decimal CostLastYear { get; set; }
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "TerritoryID: " + TerritoryID;
        public SalesTerritory() { }
        public SalesTerritory(int TerritoryID_, string Name_, string CountryRegionCode_, string Group_, int SalesYTD_, int SalesLastYear_, int CostYTD_, int CostLastYear_, Guid rowguid_, DateTime ModifiedDate_)
        {
            this.TerritoryID = TerritoryID_;
            this.Name = Name_;
            this.CountryRegionCode = CountryRegionCode_;
            this.Group = Group_;
            this.SalesYTD = SalesYTD_;
            this.SalesLastYear = SalesLastYear_;
            this.CostYTD = CostYTD_;
            this.CostLastYear = CostLastYear_;
            this.rowguid = rowguid_;
            this.ModifiedDate = ModifiedDate_;
        }
    }

}

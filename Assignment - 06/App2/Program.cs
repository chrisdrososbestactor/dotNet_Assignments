﻿using System.Configuration;
using App2.ReportService;
using App2.ReportService.Models;

namespace App2
{
    class Program
    {
        static void Main(string[] args)
        {
            var printHelper = new PrintService(new DBConfig
            {
                ConnectionString = ConfigurationManager.AppSettings["DefaultConnectionString"],
                DAL = ConfigurationManager.AppSettings["DAL"]
            });
            printHelper.PrintProcedure();
        }
    }
}

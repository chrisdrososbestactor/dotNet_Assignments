﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Printing.Models;
using Printing.Interfaces;

namespace Printing.Services
{
    public class PrintFunctions : IPrintable
    {
        public string PrintHelloWorld()
        {
            return "Hello World!";
        }
        public string PrintOrder(Order MyOrder)
        {
            return ConvertToJSON(MyOrder);
        }
        string ConvertToJSON(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

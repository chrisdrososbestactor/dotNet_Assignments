﻿using Printing.Interfaces;
using Printing.Models;
using Printing.Services;
using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Printing.UIBase
{
    public class PrintHelper
    {
        public void PrintProcedure()
        {
            var userChoice = -1;
            do
            {
                PrintMenu();
                string usrInput = Console.ReadLine();
                userChoice = CheckUserInput(usrInput);
                if (userChoice == 1)
                    PrintTypeHelloWorld();
                else if (userChoice == 2)
                    PrintTypeOrder();
                else if (userChoice == 0)
                    ScreenPrint("You didn't entered 1 or 2, try again.\r\n");
            } while (true);
        }
        public void PrintMenu()
        {
            ScreenPrint("Choose Printing type:\r\n" +
                "Enter 1 for Greetings or \r\n" +
                "2 to Print the Order (The Boss will know your choice):\r\n");
        }
        public Order CreateOrder()
        {
            var idCounter = 1;
            
            var newOrderIDList = new List<int>();
            var listOrderCompleted = false;
            string usrInput = string.Empty;

            // Creating the List with the item ID's
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            int result = -1;
            do
            {
                ScreenPrint("Give 0 as input to stop the OrderList Creation, " +
                    "any input other than a positive number that represends the item ID will be ignored.\r\n" +
                    "Enter the ID of the #" + idCounter + " product:\r\n");
                usrInput = Console.ReadLine();

                result = CheckUserInputForPositiveNumber(usrInput);
                if (result > 0)
                {
                    idCounter++;
                    newOrderIDList.Add(result);
                }
                listOrderCompleted = CreateOrderListValidator(result, newOrderIDList.Count);

            } while (!listOrderCompleted); // stops when input is 0 except when the list is empty

            // Creating ClientName      
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            var clientName = string.Empty;
            do
            {
                ScreenPrint("Enter Clients Name:(empty input is not accepted)\r\n");
                clientName = Console.ReadLine();
            } while (IsUserInputNull(clientName)); // Repeat until input recieved other than null

            // Creating EmployeeName           
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable//
            var empName = string.Empty;
            do
            {
                ScreenPrint("Enter Employee Name:(empty input is not accepted)\r\n");
                empName = Console.ReadLine();
            } while (IsUserInputNull(empName)); // Repeat until input recieved other than null
            
            // Creating Price
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            usrInput = string.Empty;
            var price = -1;
            do
            {
                ScreenPrint("Enter total Price:(only numbers >=0 are accepted)\r\n");
                usrInput = Console.ReadLine();
                price = CheckUserInputForPositiveNumber(usrInput);
            } while (price == -1); // Repeat until input was a number >= 0

            var newOrder = new Order
            {
                OrderItems = newOrderIDList,
                ClientName = clientName,
                EmployeeName = empName,
                DateOrdered = DateTime.Now,
                Price = price
            };
            return newOrder;
        }
        public void PrintTypeHelloWorld()
        {
            var printing = new PrintFunctions();
            ScreenPrint(printing.PrintHelloWorld());
        }
        public void PrintTypeOrder()
        {
            var printing = new PrintFunctions();
            var newOrder = CreateOrder();
            var printOrder = printing.PrintOrder(newOrder);
            Console.WriteLine(printOrder);
        }
        // Break List creation when the list is not empty and the input is 0
        public bool CreateOrderListValidator(int usrInput, int listCount)
        {
            if (listCount == 0)
                return false;
            else
            {
                if (usrInput == 0)
                    return true;
                else
                    return false;
            }
        }
        public int CheckUserInput(string usrInput)
        {
            var choice = 0;
            var message = string.Empty;
            Int32.TryParse(usrInput, out choice);
            if (choice != 1 && choice != 2)
                choice = 0;
            return choice;
        }
        public int CheckUserInputForPositiveNumber(string usrInput)
        {
            var choice = -1;
            bool isNum = false;
            isNum = Int32.TryParse(usrInput, out choice);
            if (isNum)
            {
                if (choice < 0)
                    choice = -1;
            }
            else
                choice = -1;
            return choice;
        }
        public bool IsUserInputNull(string usrInput)
        {
            if (usrInput == string.Empty)
                return true;
            else
                return false;
        }
        public void ScreenPrint(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}

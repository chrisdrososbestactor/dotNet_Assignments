﻿using Printing.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Printing.Interfaces
{
    public interface IPrintable
    {
        string PrintHelloWorld();
        string PrintOrder(Order order);
        
    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace MVCReports.Models
{
    public interface IReportsRepository
    {
        Task<List<EmployeeListThatLastNameStartsWithL>> GetAllEmployeeListThatLastNameStartsWithL(string dal);
        Task<List<AvgSickleaveHoursPerEmployeeMaritalStatus>> GetAllAvgSickleaveHoursPerEmployeeMaritalStatus(string dal);

        Task<List<NumEmployeesByGender>> GetAllNumEmployeesByGender(string dal);

        Task<List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders>> GetAllSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(string dal);
    }
}

﻿namespace MVCReports.Models
{
    /// <summary>
    /// Average Sick Leave Hours per Employee Marital Status (married, single)
    /// </summary>
    public class AvgSickleaveHoursPerEmployeeMaritalStatus
    {
        public string MaritalStatus { get; set; }
        public int AverageSickLeaveHours { get; set; }
    }
}

﻿namespace MVCReports.Models
{
    /// <summary>
    /// A list of employees that their last name starts with 'L'
    /// </summary>
    public class EmployeeListThatLastNameStartsWithL
    {
        public string FirtName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string PersonType { get; set; }
    }
}

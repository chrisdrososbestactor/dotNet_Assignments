﻿namespace MVCReports.Models
{
    /// <summary>
    /// A list of employees we have broken down by gender (male, female)
    /// </summary>
    public class NumEmployeesByGender
    {
        public string Gender { get; set; }
        public int Count { get; set; }
    }
}

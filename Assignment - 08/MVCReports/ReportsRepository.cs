﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MVCReports.Models
{
    public class ReportsRepository : IReportsRepository
    {
        readonly string BaseUrl = "http://localhost:50059";
        public async Task<List<EmployeeListThatLastNameStartsWithL>> GetAllEmployeeListThatLastNameStartsWithL(string dal)
        {
            var subUrl = "/Reports/EmployeeListThatLastNameStartsWithL/" + dal;
            var resultData = new List<EmployeeListThatLastNameStartsWithL>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    resultData = JsonConvert.DeserializeObject<List<EmployeeListThatLastNameStartsWithL>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            return resultData;
        }
        public async Task<List<AvgSickleaveHoursPerEmployeeMaritalStatus>> GetAllAvgSickleaveHoursPerEmployeeMaritalStatus(string dal)
        {
            var subUrl = "/Reports/AvgSickleaveHoursPerEmployeeMaritalStatus/" + dal;
            var resultData = new List<AvgSickleaveHoursPerEmployeeMaritalStatus>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    resultData = JsonConvert.DeserializeObject<List<AvgSickleaveHoursPerEmployeeMaritalStatus>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            return resultData;
        }

        public async Task<List<NumEmployeesByGender>> GetAllNumEmployeesByGender(string dal)
        {
            var subUrl = "/Reports/NumEmployeesByGender/" + dal;
            var resultData = new List<NumEmployeesByGender>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    resultData = JsonConvert.DeserializeObject<List<NumEmployeesByGender>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            return resultData;
        }

        public async Task<List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders>> GetAllSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(string dal)
        {
            var subUrl = "/Reports/SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders/" + dal;
            var resultData = new List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    resultData = JsonConvert.DeserializeObject<List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            return resultData;
        }
    }
}

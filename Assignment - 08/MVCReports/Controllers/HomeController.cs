﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCReports.Models;

namespace MVCReports.Controllers
{
    public class HomeController : Controller
    {
        readonly ILogger<HomeController> _logger;
        readonly IReportsRepository ReportsRepository;
        string ChooseDalMessage = "Choose your DAL source";
        string AvgSickleaveHoursPerEmployeeMaritalStatusName = "AvgSickleaveHoursPerEmployeeMaritalStatus";
        string AvgSickleaveHoursPerEmployeeMaritalStatusDescription = "Average Sick Leave Hours per Employee Marital Status (married, single)";
        string EmployeeListThatLastNameStartsWithLName = "EmployeeListThatLastNameStartsWithL";
        string EmployeeListThatLastNameStartsWithLDescription = "A list of employees that their last name starts with 'L'";
        string NumEmployeesByGenderName = "NumEmployeesByGender";
        string NumEmployeesByGenderDescription = "A list of employees we have broken down by gender (male, female)";
        string SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersName = "SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders";
        string SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDescription = "Sales Order Minimum and Maximum total due per territory for marketing related orders (sales reason)";

        public HomeController(ILogger<HomeController> logger, IReportsRepository reportsRepository)
        {
            _logger = logger;
            ReportsRepository = reportsRepository;
        }
        [HttpGet]
        public async Task<IActionResult> AvgSickleaveHoursPerEmployeeMaritalStatus(string dal = "ADO")
        {
            ViewBag.ChooseDal = ChooseDalMessage;
            ViewBag.Title = AvgSickleaveHoursPerEmployeeMaritalStatusName;
            ViewBag.Description = AvgSickleaveHoursPerEmployeeMaritalStatusDescription;

            var data = await ReportsRepository.GetAllAvgSickleaveHoursPerEmployeeMaritalStatus(dal);
            return View(data);
        }
        [HttpGet]
        public async Task<IActionResult> EmployeeListThatLastNameStartsWithL(string dal = "ADO")
        {
            ViewBag.ChooseDal = ChooseDalMessage;
            ViewBag.Title = EmployeeListThatLastNameStartsWithLName;
            ViewBag.Description = EmployeeListThatLastNameStartsWithLDescription;

            var data = await ReportsRepository.GetAllEmployeeListThatLastNameStartsWithL(dal);
            return View(data);
        }
        [HttpGet]
        public async Task<IActionResult> NumEmployeesByGender(string dal = "ADO")
        {
            ViewBag.ChooseDal = ChooseDalMessage;
            ViewBag.Title = NumEmployeesByGenderName;
            ViewBag.Description = NumEmployeesByGenderDescription;

            var data = await ReportsRepository.GetAllNumEmployeesByGender(dal);
            return View(data);
        }
        [HttpGet]
        public async Task<IActionResult> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(string dal = "ADO")
        {
            ViewBag.ChooseDal = ChooseDalMessage;
            ViewBag.Title = SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersName;
            ViewBag.Description = SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDescription;

            var data = await ReportsRepository.GetAllSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(dal);
            return View(data);
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}

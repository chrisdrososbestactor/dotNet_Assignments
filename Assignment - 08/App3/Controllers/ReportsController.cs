﻿using System.Linq;
using App3.ReportService;
using App3.ReportService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace App3.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportsController : ControllerBase
    {
        readonly ILogger<ReportsController> _logger;
        IPrintService PrintService;
        PrintFunctions PrintFunctions;
        AppSettings Settings;
        public ReportsController(ILogger<ReportsController> logger
            , IPrintService printService, PrintFunctions printFunctions, IOptions<AppSettings> settings)
        {
            Settings = settings.Value;
            PrintService = printService;
            PrintFunctions = printFunctions;
            _logger = logger;
        }
        [HttpGet("NumEmployeesByGender/{dal}")]
        public IActionResult GetNumEmployeesByGender(string dal)
        {
            var config = new App3.Models.DalConfig()
            {
                ConnectionString = Settings.ConnectionString,
                DAL = dal
            };
            var task = PrintService.GetNumEmployeesByGender(config).ToList();
            if (task.Count == 0)
                return NotFound();
            return Ok(PrintFunctions.ConvertToJSON(task));
        }
        [HttpGet("SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders/{dal}")]
        public IActionResult GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(string dal)
        {
            var config = new App3.Models.DalConfig()
            {
                ConnectionString = Settings.ConnectionString,
                DAL = dal
            };
            var task = PrintService.GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(config).ToList();
            if (task.Count == 0)
                return NotFound();
            return Ok(PrintFunctions.ConvertToJSON(task));
        }
        [HttpGet("AvgSickleaveHoursPerEmployeeMaritalStatus/{dal}")]
        public IActionResult GetAvgSickleaveHoursPerEmployeeMaritalStatus(string dal)
        {
            var config = new App3.Models.DalConfig()
            {
                ConnectionString = Settings.ConnectionString,
                DAL = dal
            };
            var task = PrintService.GetAvgSickleaveHoursPerEmployeeMaritalStatus(config).ToList();
            if (task.Count == 0)
                return NotFound();
            return Ok(PrintFunctions.ConvertToJSON(task));
        }
        [HttpGet("EmployeeListThatLastNameStartsWithL/{dal}")]
        public IActionResult GetEmployeeListThatLastNameStartsWithL(string dal)
        {
            var config = new App3.Models.DalConfig()
            {
                ConnectionString = Settings.ConnectionString,
                DAL = dal
            };
            var task = PrintService.GetEmployeeListThatLastNameStartsWithL(config).ToList();
            if (task.Count == 0)
                return NotFound();
            return Ok(PrintFunctions.ConvertToJSON(task));
        }
    }
}

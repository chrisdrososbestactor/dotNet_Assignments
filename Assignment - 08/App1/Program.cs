﻿using System.Configuration;
using App1.ReportService;
using App1.ReportService.Models;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {
            var printHelper = new PrintService(new DBConfig
            {
                ConnectionString = ConfigurationManager.AppSettings["DefaultConnectionString"],
                DAL = ConfigurationManager.AppSettings["DAL"]
            });
            printHelper.PrintProcedure();
        }
    }
}

﻿using System;

namespace App1.ReportService.Models
{
    public class GetNumEmployeesByGender
    {
        public string Gender { get; set; }
        public int Count { get; set; }
    }
}

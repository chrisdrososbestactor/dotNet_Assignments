﻿using System;

namespace App1.ReportService.Models
{
    public class AvgSickleaveHoursPerEmployeeMaritalStatus
    {
        public string MaritalStatus { get; set; }
        public int AverageSickLeaveHours { get; set; }
    }
}

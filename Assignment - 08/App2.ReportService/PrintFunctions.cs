﻿using App2.ReportService.Interfaces;
using App2.ReportService.Models;
using Newtonsoft.Json;

namespace App2.ReportService
{
    public class PrintFunctions : IPrintable
    {
        public string PrintHelloWorld()
        {
            return "Hello World!";
        }
        public string PrintOrder(Order order)
        {
            return ConvertToJSON(order);
        }
        public string ConvertToJSON(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

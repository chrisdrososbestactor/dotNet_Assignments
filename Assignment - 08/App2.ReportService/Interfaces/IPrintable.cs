﻿using App2.ReportService.Models;

namespace App2.ReportService.Interfaces
{
    public interface IPrintable
    {
        string PrintHelloWorld();
        string PrintOrder(Order order);

    }
}

﻿using System;
using CoreReportService.Models;
using CoreReportService.Interfaces;
using DAL.Interfaces;

namespace CoreReportService
{
    /// <summary>
    /// Creates a TaskProvider for the CoreReportService.DataReports
    /// </summary>
    public class DefaultTaskProviderFactory : ITaskProviderFactory
    {
        readonly DalConfig Config;

        public DefaultTaskProviderFactory(DalConfig config)
        {
            Config = config;
        }

        public ITaskEnumerableProvider GetTaskProvider()
        {
            switch (Config.DAL)
            {
                case "ADO":
                    return new ADOTaskProvider.Tasks(Config.ConnectionString);
                case "EFCore":
                    return new EFCoreTaskProvider.Tasks(Config.ConnectionString);
                default:
                    throw new FormatException("The format of the variable which represent the selected DAL was not correct");
            }
        }
    }
}

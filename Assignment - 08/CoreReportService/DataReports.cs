﻿using System.Collections.Generic;
using System.Linq;
using CoreReportService.Models;
using CoreReportService.Interfaces;
using DAL.Interfaces;

namespace CoreReportService
{
    public class DataReports : IDataReports
    {
        readonly ITaskProviderFactory TaskProviderFactory;
        readonly string ConnectionString;
        readonly string DalModeSelected;
        public DataReports() { }
        public DataReports(ITaskProviderFactory factory)
        {
            TaskProviderFactory = factory;
        }
        public DataReports(DalConfig config)
        {
            ConnectionString = config.ConnectionString;
            DalModeSelected = config.DAL;
        }
        ITaskEnumerableProvider GetTaskProvider()
        {
            return TaskProviderFactory.GetTaskProvider();
        }
        public IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
        {
            return GetTaskProvider().SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
                .Select(x => new CoreReportService.Models.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
                {
                    MaxTotalDue = x.MaxTotalDue,
                    MinTotalDue = x.MinTotalDue,
                    TerritoryName = x.TerritoryName
                })
                .ToList();
        }
        public IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus()
        {
            return GetTaskProvider().AvgSickleaveHoursPerEmployeeMaritalStatus()
                .Select(x => new CoreReportService.Models.AvgSickleaveHoursPerEmployeeMaritalStatus()
                {
                    AverageSickLeaveHours = x.AverageSickLeaveHours,
                    MaritalStatus = x.MaritalStatus
                })
                .ToList();
        }
        public IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL()
        {
            return GetTaskProvider().EmployeeListThatLastNameStartsWithL()
                .Select(x => new CoreReportService.Models.EmployeeListThatLastNameStartsWithL()
                {
                    EmailAddress = x.EmailAddress,
                    FirtName = x.FirtName,
                    LastName = x.LastName,
                    MiddleName = x.MiddleName,
                    PersonType = x.PersonType,
                }).ToList();
        }
        public IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender()
        {
            return GetTaskProvider().GetNumEmployeesByGender()
                .Select(x => new CoreReportService.Models.GetNumEmployeesByGender()
                {
                    Count = x.Count,
                    Gender = x.Gender,
                }).ToList();
        }
    }
}

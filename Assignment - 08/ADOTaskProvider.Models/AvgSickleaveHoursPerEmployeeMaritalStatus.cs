﻿using System;

namespace ADOTaskProvider.Models
{
    public class AvgSickleaveHoursPerEmployeeMaritalStatus
    {
        public string MaritalStatus { get; set; }
        public int AverageSickLeaveHours { get; set; }
    }
}

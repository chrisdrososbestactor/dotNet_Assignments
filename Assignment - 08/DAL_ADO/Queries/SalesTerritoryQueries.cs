﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class SalesTerritoryQueries
    {
        public string GetAllFromSalesTerritoryQuery()
        {
            return @"SELECT 
                   [TerritoryID]
                  ,[Name]
                  ,[CountryRegionCode]
                  ,[Group]
                  ,[SalesYTD]
                  ,[SalesLastYear]
                  ,[CostYTD]
                  ,[CostLastYear]
                  ,[rowguid]
                  ,[ModifiedDate]
              FROM [Sales].[SalesTerritory]";
        }
    }
}

﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class SalesOrderHeaderSalesReasonsRepo : IDataReader<SalesOrderHeaderSalesReason>
    {
        public IEnumerable<SalesOrderHeaderSalesReason> GetData(string connectionString)
        {
            var queries = new SalesOrderHeaderSalesReasonQueries();
            var salesOrderHeaderSalesReasonList = new List<SalesOrderHeaderSalesReason>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queries.GetAllFromSalesOrderHeaderSalesReasonQuery(), connection))
                { 
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var salesOrderHeaderSalesReason = new SalesOrderHeaderSalesReason()
                            {
                                SalesOrderId = reader.GetInt32(0),
                                SalesReasonId = reader.GetInt32(1),
                                ModifiedDate = reader.GetDateTime(2)
                            };
                            salesOrderHeaderSalesReasonList.Add(salesOrderHeaderSalesReason);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return salesOrderHeaderSalesReasonList;
        }
    }
}

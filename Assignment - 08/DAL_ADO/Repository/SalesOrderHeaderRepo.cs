﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class SalesOrderHeaderRepo : IDataReader<SalesOrderHeader>
    {
        public IEnumerable<SalesOrderHeader> GetData(string connectionString)
        {
            var queries = new SalesOrderHeaderQueries();
            var salesOrderHeaderList = new List<SalesOrderHeader>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queries.GetAllFromSalesOrderHeaderQuery(), connection))
                { 
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var salesOrderHeader = new SalesOrderHeader()
                            {
                                SalesOrderId = reader.GetInt32(0),
                                RevisionNumber = reader.GetByte(1), 
                                OrderDate = reader.GetDateTime(2),
                                DueDate = reader.GetDateTime(3),
                                ShipDate = reader.GetDateTime(4),
                                Status = reader.GetByte(5),
                                OnlineOrderFlag = reader.GetBoolean(6),
                                SalesOrderNumber = reader.GetString(7),
                                PurchaseOrderNumber = reader[8] as string,
                                AccountNumber = reader[9] as string,
                                CustomerId = reader.GetInt32(10),
                                SalesPersonId = reader[11] as int?,
                                TerritoryId = reader.GetInt32(12),
                                BillToAddressId = reader.GetInt32(13),
                                ShipToAddressId = reader.GetInt32(14),
                                ShipMethodId = reader.GetInt32(15),
                                CreditCardId = reader[16] as int?,
                                CreditCardApprovalCode = reader[17] as string,
                                CurrencyRateId = reader[18] as int?,
                                SubTotal = reader.GetDecimal(19),
                                TaxAmt = reader.GetDecimal(20),
                                Freight = reader.GetDecimal(21),
                                TotalDue = reader.GetDecimal(22),
                                Comment = reader[23] as string,
                                Rowguid = reader.GetGuid(24),
                                ModifiedDate = reader.GetDateTime(25),
                            };
                            salesOrderHeaderList.Add(salesOrderHeader);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return salesOrderHeaderList;
        }
    }
}

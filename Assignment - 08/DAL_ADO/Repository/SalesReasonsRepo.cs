﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class SalesReasonsRepo : IDataReader<SalesReason>
    {
        public IEnumerable<SalesReason> GetData(string connectionString)
        {
            var queries = new SalesReasonQueries();
            var salesReasonList = new List<SalesReason>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queries.GetAllFromSalesReasonQuery(), connection))
                { 
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var salesReason = new SalesReason()
                            {
                                SalesReasonId = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                ReasonType = reader.GetString(2),
                                ModifiedDate = reader.GetDateTime(3)
                            };
                            salesReasonList.Add(salesReason);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return salesReasonList;
        }
    }
}

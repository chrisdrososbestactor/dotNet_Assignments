﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADO.Interfaces
{
    public interface IDataReader<T>
    {
        IEnumerable<T> GetData(string connectionString);        
    }
}

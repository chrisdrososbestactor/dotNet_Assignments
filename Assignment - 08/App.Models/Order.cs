﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App.Domain
{
    public class Order
    {
        // a List with the IDs of the Order Items
        public List<int> OrderItems { get; set; } = new List<int>();
        public string ClientName { get; set; } = String.Empty;
        public DateTime DateOrdered { get; set; }
        public string EmployeeName { get; set; } = String.Empty;
        public int Price { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBModels.ADO
{    
    public class Person
    {
        public int BusinessEntityID { get; set; }
        public string PersonType { get; set; } = String.Empty;
        public string NameStyle { get; set; } = String.Empty;
        public string? Title { get; set; }
        public string FirstName { get; set; } = String.Empty;
        public string? MiddleName { get; set; }
        public string LastName { get; set; } = String.Empty;
        public string? Suffix { get; set; } = String.Empty;
        public int EmailPromotion { get; set; }
        public string? AdditionalContactInfo { get; set; }
        public string? Demographics { get; set; }
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "BusinessEntityID: " + BusinessEntityID;


    }
    
}

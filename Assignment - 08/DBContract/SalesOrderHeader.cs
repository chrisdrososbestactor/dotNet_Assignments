﻿using System;

namespace DBModels.ADO
{
    public class SalesOrderHeader
    {
        public int SalesOrderID { get; set; }
        public byte RevisionNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DueDate { get; set; }
        public DateTime? ShipDate { get; set; }
        public byte Status { get; set; }
        public bool OnlineOrderFlag { get; set; }
        public string SalesOrderNumber { get; set; } = String.Empty;
        public string? PurchaseOrderNumber { get; set; }
        public string? AccountNumber { get; set; }
        public int CustomerID { get; set; }
        public int? SalesPersonID { get; set; }
        public int? TerritoryID { get; set; }
        public int BillToAddressID { get; set; }
        public int ShipToAddressID { get; set; }
        public int ShipMethodID { get; set; }
        public int? CreditCardID { get; set; }
        public string? CreditCardApprovalCode { get; set; }
        public int? CurrencyRateID { get; set; }
        public decimal SubTotal { get; set; }
        public decimal TaxAmt { get; set; }
        public decimal Freight { get; set; }
        public decimal TotalDue { get; set; }
        public string? Comment { get; set; }
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "SalesOrderID: " + SalesOrderID;
        public SalesOrderHeader() { }
        public SalesOrderHeader(int SalesOrderID_, byte RevisionNumber_, DateTime OrderDate_, DateTime DueDate_, DateTime? ShipDate_, byte Status_, bool OnlineOrderFlag_, string SalesOrderNumber_, string? PurchaseOrderNumber_, string? AccountNumber_, int CustomerID_, int? SalesPersonID_, int? TerritoryID_, int BillToAddressID_, int ShipToAddressID_, int ShipMethodID_, int? CreditCardID_, string? CreditCardApprovalCode_, int? CurrencyRateID_, int SubTotal_, int TaxAmt_, int Freight_, int TotalDue_, string Comment_, Guid rowguid_, DateTime ModifiedDate_)
        {
            this.SalesOrderID = SalesOrderID_;
            this.RevisionNumber = RevisionNumber_;
            this.OrderDate = OrderDate_;
            this.DueDate = DueDate_;
            this.ShipDate = ShipDate_;
            this.Status = Status_;
            this.OnlineOrderFlag = OnlineOrderFlag_;
            this.SalesOrderNumber = SalesOrderNumber_;
            this.PurchaseOrderNumber = PurchaseOrderNumber_;
            this.AccountNumber = AccountNumber_;
            this.CustomerID = CustomerID_;
            this.SalesPersonID = SalesPersonID_;
            this.TerritoryID = TerritoryID_;
            this.BillToAddressID = BillToAddressID_;
            this.ShipToAddressID = ShipToAddressID_;
            this.ShipMethodID = ShipMethodID_;
            this.CreditCardID = CreditCardID_;
            this.CreditCardApprovalCode = CreditCardApprovalCode_;
            this.CurrencyRateID = CurrencyRateID_;
            this.SubTotal = SubTotal_;
            this.TaxAmt = TaxAmt_;
            this.Freight = Freight_;
            this.TotalDue = TotalDue_;
            this.Comment = Comment_;
            this.rowguid = rowguid_;
            this.ModifiedDate = ModifiedDate_;
        }
    }
    
}

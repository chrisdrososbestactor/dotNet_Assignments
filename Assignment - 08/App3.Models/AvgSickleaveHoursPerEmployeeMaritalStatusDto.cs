﻿namespace App3.Models
{
    /// <summary>
    /// Average Sick Leave Hours per Employee Marital Status (married, single)
    /// </summary>
    public class AvgSickleaveHoursPerEmployeeMaritalStatusDto
    {
        public string MaritalStatus { get; set; }
        public int AverageSickLeaveHours { get; set; }
    }
}

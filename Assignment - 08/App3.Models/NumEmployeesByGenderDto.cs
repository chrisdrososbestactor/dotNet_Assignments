﻿namespace App3.Models
{
    /// <summary>
    /// A list of employees we have broken down by gender (male, female)
    /// </summary>
    public class NumEmployeesByGenderDto
    {
        public string Gender { get; set; }
        public int Count { get; set; }
    }
}

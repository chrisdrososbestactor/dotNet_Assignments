﻿using App3.Models;
using App3.ReportService.Interfaces;
using Newtonsoft.Json;

namespace App3.ReportService
{
    public class PrintFunctions : IPrintable
    {
        public string PrintHelloWorld()
        {
            return "Hello World!";
        }
        public string PrintOrder(Order order)
        {
            return ConvertToJSON(order);
        }
        public string ConvertToJSON(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using App3.Models;
using App3.ReportService.Interfaces;
using CoreReportService;
using CoreReportService.Models;
using DalConfig = App3.Models.DalConfig;

namespace App3.ReportService
{
    public class PrintService : IPrintService
    {
        public PrintService() { }
        public List<EmployeeListThatLastNameStartsWithLDto> GetEmployeeListThatLastNameStartsWithL(DalConfig config)
        {
            var dalConfig = new CoreReportService.Models.DalConfig
            {
                DAL = config.DAL,
                ConnectionString = config.ConnectionString
            };
            var taskProviderfactory = new DefaultTaskProviderFactory(dalConfig);
            var dataReports = new DataReports(taskProviderfactory);
            var results = dataReports.EmployeeListThatLastNameStartsWithL().ToList();
            var dtoList = new List<EmployeeListThatLastNameStartsWithLDto>();
            foreach (EmployeeListThatLastNameStartsWithL result in results)
            {
                var dto = new EmployeeListThatLastNameStartsWithLDto()
                {
                    EmailAddress = result.EmailAddress,
                    FirtName = result.FirtName,
                    LastName = result.LastName,
                    MiddleName = result.MiddleName,
                    PersonType = result.PersonType,
                };
                dtoList.Add(dto);
            }
            return dtoList;
        }
        public List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto> GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(DalConfig config)
        {
            var dalConfig = new CoreReportService.Models.DalConfig
            {
                DAL = config.DAL,
                ConnectionString = config.ConnectionString
            };
            var taskProviderfactory = new DefaultTaskProviderFactory(dalConfig);
            var dataReports = new DataReports(taskProviderfactory);
            var results = dataReports.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders().ToList();
            var dtoList = new List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto>();
            foreach (SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders result in results)
            {
                var dto = new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto()
                {
                    MaxTotalDue = result.MaxTotalDue,
                    MinTotalDue = result.MinTotalDue,
                    TerritoryName = result.TerritoryName
                };
                dtoList.Add(dto);
            }
            return dtoList;
        }
        public List<AvgSickleaveHoursPerEmployeeMaritalStatusDto> GetAvgSickleaveHoursPerEmployeeMaritalStatus(DalConfig config)
        {
            var dalConfig = new CoreReportService.Models.DalConfig
            {
                DAL = config.DAL,
                ConnectionString = config.ConnectionString
            };
            var taskProviderfactory = new DefaultTaskProviderFactory(dalConfig);
            var dataReports = new DataReports(taskProviderfactory);
            var results = dataReports.AvgSickleaveHoursPerEmployeeMaritalStatus().ToList();
            var dtoList = new List<AvgSickleaveHoursPerEmployeeMaritalStatusDto>();
            foreach (AvgSickleaveHoursPerEmployeeMaritalStatus result in results)
            {
                var dto = new AvgSickleaveHoursPerEmployeeMaritalStatusDto()
                {
                    AverageSickLeaveHours = result.AverageSickLeaveHours,
                    MaritalStatus = result.MaritalStatus
                };
                dtoList.Add(dto);
            }
            return dtoList;
        }
        public List<NumEmployeesByGenderDto> GetNumEmployeesByGender(DalConfig config)
        {
            var dalConfig = new CoreReportService.Models.DalConfig
            {
                DAL = config.DAL,
                ConnectionString = config.ConnectionString
            };
            var taskProviderfactory = new DefaultTaskProviderFactory(dalConfig);
            var dataReports = new DataReports(taskProviderfactory);
            var results = dataReports.GetNumEmployeesByGender().ToList();
            var dtoList = new List<NumEmployeesByGenderDto>();
            foreach (GetNumEmployeesByGender result in results)
            {
                var dto = new NumEmployeesByGenderDto()
                {
                    Count = result.Count,
                    Gender = result.Gender
                };
                dtoList.Add(dto);
            }
            return dtoList;
        }
    }
}

﻿using System.Collections.Generic;
using App3.Models;

namespace App3.ReportService.Interfaces
{
    public interface IPrintService
    {
        List<EmployeeListThatLastNameStartsWithLDto> GetEmployeeListThatLastNameStartsWithL(DalConfig config);
        List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto> GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(DalConfig config);
        List<AvgSickleaveHoursPerEmployeeMaritalStatusDto> GetAvgSickleaveHoursPerEmployeeMaritalStatus(DalConfig config);

        List<NumEmployeesByGenderDto> GetNumEmployeesByGender(DalConfig config);

    }
}

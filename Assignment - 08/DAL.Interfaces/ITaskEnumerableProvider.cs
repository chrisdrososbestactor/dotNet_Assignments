﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Models;

namespace DAL.Interfaces
{
    public interface ITaskEnumerableProvider
    {
        IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

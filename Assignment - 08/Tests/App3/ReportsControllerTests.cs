﻿using App3;
using App3.Controllers;
using App3.ReportService.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests
{
    [TestClass]
    public class ReportsControllerTests
    {
        [TestMethod]
        [Description("Checks if the Reports Controller is generating any data from the Report")]
        public void GetAvgSickleaveHoursPerEmployeeMaritalStatusFromControllerTest()
        {
            // Create mock configuration files for every class
            AppSettings settings = new AppSettings()
            {
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };
            App3.Models.DalConfig config = new App3.Models.DalConfig()
            {
                DAL = "ADO",
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };

            // Create mock options, PrintService and logger
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(op => op.Value).Returns(settings);
            var printFunction = new App3.ReportService.PrintFunctions();
            var loger = new Logger<ReportsController>(new LoggerFactory());

            var mockPrintService = new Mock<App3.ReportService.PrintService>().As<IPrintService>();
            mockPrintService.CallBase = true;
            mockPrintService.Setup(x => x.GetAvgSickleaveHoursPerEmployeeMaritalStatus(config)).Returns(new App3MockDatabaseRecordsProvider().GetAllMockAvgSickleaveHoursPerEmployeeMaritalStatusDtos);

            // Create mock controller
            var testController = new ReportsController(loger, mockPrintService.Object, printFunction, mockOptions.Object);
            var result = testController.GetAvgSickleaveHoursPerEmployeeMaritalStatus("ADO") as OkObjectResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result.Value != null);
        }
        [TestMethod]
        [Description("Checks if the Reports Controller is generating any data from the Report")]
        public void GetEmployeeListThatLastNameStartsWithLFromControllerTest()
        {
            // Create mock configuration files for every class
            AppSettings settings = new AppSettings()
            {
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };
            App3.Models.DalConfig config = new App3.Models.DalConfig()
            {
                DAL = "ADO",
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };

            // Create mock options, PrintService and logger
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(op => op.Value).Returns(settings);
            var printFunction = new App3.ReportService.PrintFunctions();
            var loger = new Logger<ReportsController>(new LoggerFactory());

            var mockPrintService = new Mock<App3.ReportService.PrintService>().As<IPrintService>();
            mockPrintService.CallBase = true;
            mockPrintService.Setup(x => x.GetEmployeeListThatLastNameStartsWithL(config)).Returns(new App3MockDatabaseRecordsProvider().GetAllMockEmployeeListThatLastNameStartsWithLDtos);

            // Create mock controller
            var testController = new ReportsController(loger, mockPrintService.Object, printFunction, mockOptions.Object);
            var result = testController.GetEmployeeListThatLastNameStartsWithL("ADO") as OkObjectResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result.Value != null);
        }
        [TestMethod]
        [Description("Checks if the Reports Controller is generating any data from the Report")]
        public void GetNumEmployeesByGenderFromControllerTest()
        {
            // Create mock configuration files for every class
            AppSettings settings = new AppSettings()
            {
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };
            App3.Models.DalConfig config = new App3.Models.DalConfig()
            {
                DAL = "ADO",
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };

            // Create mock options, PrintService and logger
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(op => op.Value).Returns(settings);
            var printFunction = new App3.ReportService.PrintFunctions();
            var loger = new Logger<ReportsController>(new LoggerFactory());

            var mockPrintService = new Mock<App3.ReportService.PrintService>().As<IPrintService>();
            mockPrintService.CallBase = true;
            mockPrintService.Setup(x => x.GetNumEmployeesByGender(config)).Returns(new App3MockDatabaseRecordsProvider().GetAllMockNumEmployeesByGenderDtos);

            // Create mock controller
            var testController = new ReportsController(loger, mockPrintService.Object, printFunction, mockOptions.Object);
            var result = testController.GetNumEmployeesByGender("ADO") as OkObjectResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result.Value != null);
        }
        [TestMethod]
        [Description("Checks if the Reports Controller is generating any data from the Report")]
        public void GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersFromControllerTest()
        {
            // Create mock configuration files for every class
            AppSettings settings = new AppSettings()
            {
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };
            App3.Models.DalConfig config = new App3.Models.DalConfig()
            {
                DAL = "ADO",
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };

            // Create mock options, PrintService and logger
            var mockOptions = new Mock<IOptions<AppSettings>>();
            mockOptions.Setup(op => op.Value).Returns(settings);
            var printFunction = new App3.ReportService.PrintFunctions();
            var loger = new Logger<ReportsController>(new LoggerFactory());

            var mockPrintService = new Mock<App3.ReportService.PrintService>().As<IPrintService>();
            mockPrintService.CallBase = true;
            mockPrintService.Setup(x => x.GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders(config)).Returns(new App3MockDatabaseRecordsProvider().GetAllMockSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDtos);

            // Create mock controller
            var testController = new ReportsController(loger, mockPrintService.Object, printFunction, mockOptions.Object);
            var result = testController.GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders("ADO") as OkObjectResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result.Value != null);
        }
    }
}

﻿using System.Collections.Generic;
using App3.Models;

namespace Tests
{
    /// <summary>
    /// Returns Mock tables with 1 record in each one
    /// </summary>
    public class App3MockDatabaseRecordsProvider
    {
        public List<EmployeeListThatLastNameStartsWithLDto> GetAllMockEmployeeListThatLastNameStartsWithLDtos()
        {
            return new List<EmployeeListThatLastNameStartsWithLDto>()
            {
                new EmployeeListThatLastNameStartsWithLDto
                {
                    EmailAddress = "fake@email.gr",
                    FirtName = "L",
                    LastName = "Lakis",
                    MiddleName = "M",
                    PersonType = "EM"
                }

            };
        }
        public List<NumEmployeesByGenderDto> GetAllMockNumEmployeesByGenderDtos()
        {
            return new List<NumEmployeesByGenderDto>()
            {
                new NumEmployeesByGenderDto
                {
                    Count = 0,
                    Gender = "M"
                }

            };
        }
        public List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto> GetAllMockSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDtos()
        {
            return new List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto>()
            {
                new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto
                {
                    MaxTotalDue = 0,
                    MinTotalDue = 0,
                    TerritoryName = "America"

                }

            };
        }
        public List<AvgSickleaveHoursPerEmployeeMaritalStatusDto> GetAllMockAvgSickleaveHoursPerEmployeeMaritalStatusDtos()
        {
            return new List<AvgSickleaveHoursPerEmployeeMaritalStatusDto>()
            {
                new AvgSickleaveHoursPerEmployeeMaritalStatusDto
                {
                    AverageSickLeaveHours = 1,
                    MaritalStatus = "S"
                }

            };
        }
    }
}

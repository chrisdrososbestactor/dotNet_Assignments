﻿using System;
using System.Linq;
using CoreReportService;
using CoreReportService.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Tests
{
    [TestClass]
    public class EFCoreDataReportsTests
    {
        //string ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017";
        void InsertData(DbContextOptions<DAL_EFCore.AdventureWorks2017Context> options)
        {
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                context.Person.Add(new DBModels.Person
                {
                    BusinessEntityId = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                    PersonType = "EM",
                    NameStyle = false,
                    Title = "Hacker",
                    FirstName = "John",
                    MiddleName = "Doeish",
                    LastName = "Lamborgini",
                    Suffix = "S",
                    EmailPromotion = 1,
                    AdditionalContactInfo = "None",
                    Demographics = "None",
                });
                context.EmailAddress1.Add(new DBModels.EmailAddress1
                {
                    BusinessEntityId = 1,
                    EmailAddressId = 1,
                    EmailAddress = "no@email.man",
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.Employee.Add(new DBModels.Employee
                {
                    OrganizationLevel = 1,
                    SalariedFlag = false,
                    CurrentFlag = false,
                    BusinessEntityId = 1,
                    NationalIdnumber = "GR",
                    LoginId = "login",
                    JobTitle = "hacker",
                    BirthDate = new DateTime(1983, 7, 7),
                    MaritalStatus = "M",
                    Gender = "M",
                    HireDate = new DateTime(2003, 7, 7),
                    VacationHours = 1,
                    SickLeaveHours = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SalesOrderHeaderSalesReason.Add(new DBModels.SalesOrderHeaderSalesReason
                {
                    SalesOrderId = 1,
                    SalesReasonId = 1,
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SalesReason.Add(new DBModels.SalesReason
                {
                    Name = "Reason",
                    SalesReasonId = 1,
                    ReasonType = "Marketing",
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SalesTerritory.Add(new DBModels.SalesTerritory
                {
                    TerritoryId = 1,
                    Name = "DrosoArea",
                    CountryRegionCode = "GR",
                    Group = "EU",
                    SalesYtd = 1,
                    SalesLastYear = 1,
                    CostYtd = 1,
                    CostLastYear = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SalesOrderHeader.Add(new DBModels.SalesOrderHeader
                {
                    SalesOrderId = 1,
                    RevisionNumber = 17,
                    OrderDate = new DateTime(2018, 7, 7),
                    DueDate = new DateTime(2018, 7, 7),
                    ShipDate = new DateTime(2018, 7, 7),
                    Status = 0,
                    OnlineOrderFlag = true,
                    SalesOrderNumber = "1",
                    PurchaseOrderNumber = "1",
                    AccountNumber = "1",
                    CustomerId = 1,
                    SalesPersonId = 1,
                    TerritoryId = 1,
                    BillToAddressId = 1,
                    ShipToAddressId = 1,
                    ShipMethodId = 1,
                    CreditCardId = 1,
                    CreditCardApprovalCode = "zZz",
                    CurrencyRateId = 1,
                    SubTotal = 1,
                    TaxAmt = 1,
                    Freight = 1,
                    TotalDue = 10,
                    Comment = "Bad Client, eliminate",
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SaveChanges();
            }
        }
        [TestMethod]
        public void AvgSickleaveHoursPerEmployeeMaritalStatusTest()
        {
            // Arrange
            // Setup Mock Data and context
            var options = new DbContextOptionsBuilder<DAL_EFCore.AdventureWorks2017Context>()
                .UseInMemoryDatabase(databaseName: "AvgSickleaveHoursPerEmployeeMaritalStatusTest")
                .Options;
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                InsertData(options);
            }
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                // Actual EFCoreTaskProvider.Tasks targeting in-memory database
                var taskProvider = new EFCoreTaskProvider.Tasks(context);

                // Mock factory configured to return the desired provider
                var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                    temp.GetTaskProvider() == taskProvider // Return the actual provider for testing
                );

                // CoreReportService.DataReports with InMemoryDatabase
                var dataReports = new DataReports(mockFactory);

                //Act
                var result = dataReports.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders().ToList();

                //Assert
                Assert.IsTrue(result.Count == 1);
            }

        }
        [TestMethod]
        public void EmployeeListThatLastNameStartsWithLTest()
        {
            // Arrange
            // Setup Mock Data and context
            var options = new DbContextOptionsBuilder<DAL_EFCore.AdventureWorks2017Context>()
                .UseInMemoryDatabase(databaseName: "EmployeeListThatLastNameStartsWithLTest")
                .Options;
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                InsertData(options);
            }
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                // Actual EFCoreTaskProvider.Tasks targeting in-memory database
                var taskProvider = new EFCoreTaskProvider.Tasks(context);

                // Mock factory configured to return the desired provider
                var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                    temp.GetTaskProvider() == taskProvider // Return the actual provider for testing
                );

                // CoreReportService.DataReports with InMemoryDatabase
                var dataReports = new DataReports(mockFactory);

                // Act
                var result = dataReports.EmployeeListThatLastNameStartsWithL().ToList();
                // Assert
                Assert.IsTrue(result.Count == 1);
            }
        }
        [TestMethod]
        public void GetNumEmployeesByGenderTest()
        {
            // Arrange
            // Setup Mock Data and context
            var options = new DbContextOptionsBuilder<DAL_EFCore.AdventureWorks2017Context>()
                .UseInMemoryDatabase(databaseName: "GetNumEmployeesByGenderTest")
                .Options;
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                InsertData(options);
            }
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                // Actual EFCoreTaskProvider.Tasks targeting in-memory database
                var taskProvider = new EFCoreTaskProvider.Tasks(context);

                // Mock factory configured to return the desired provider
                var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                    temp.GetTaskProvider() == taskProvider // Return the actual provider for testing
                );

                // CoreReportService.DataReports with InMemoryDatabase
                var dataReports = new DataReports(mockFactory);

                // Act
                var result = dataReports.GetNumEmployeesByGender().ToList();
                // Assert
                Assert.IsTrue(result.Count == 1);
            }
        }
        [TestMethod]
        public void SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersTest()
        {
            // Arrange
            // Setup Mock Data and context
            var options = new DbContextOptionsBuilder<DAL_EFCore.AdventureWorks2017Context>()
                .UseInMemoryDatabase(databaseName: "SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersTest")
                .Options;
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                InsertData(options);
            }
            using (var context = new DAL_EFCore.AdventureWorks2017Context(options))
            {
                // Actual EFCoreTaskProvider.Tasks targeting in-memory database
                var taskProvider = new EFCoreTaskProvider.Tasks(context);

                // Mock factory configured to return the desired provider
                var mockFactory = Mock.Of<ITaskProviderFactory>(temp =>
                    temp.GetTaskProvider() == taskProvider // Return the actual provider for testing
                );

                // CoreReportService.DataReports with InMemoryDatabase
                var dataReports = new DataReports(mockFactory);

                // Act
                var result = dataReports.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders().ToList();
                // Assert
                Assert.IsTrue(result.Count == 1);
            }
        }
    }
}

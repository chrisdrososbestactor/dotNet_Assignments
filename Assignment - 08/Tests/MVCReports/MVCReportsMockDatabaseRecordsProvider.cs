﻿using System.Collections.Generic;
using MVCReports.Models;

namespace Tests
{
    /// <summary>
    /// Returns Mock tables with 1 record in each one
    /// </summary>
    public class MVCReportsMockDatabaseRecordsProvider
    {
        public List<EmployeeListThatLastNameStartsWithL> GetAllMockEmployeeListThatLastNameStartsWithL()
        {
            return new List<EmployeeListThatLastNameStartsWithL>()
            {
                new EmployeeListThatLastNameStartsWithL
                {
                    EmailAddress = "fake@email.gr",
                    FirtName = "L",
                    LastName = "Lakis",
                    MiddleName = "M",
                    PersonType = "EM"
                }

            };
        }
        public List<NumEmployeesByGender> GetAllMockNumEmployeesByGenders()
        {
            return new List<NumEmployeesByGender>()
            {
                new NumEmployeesByGender
                {
                    Count = 0,
                    Gender = "M"
                }

            };
        }
        public List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> GetAllMockSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
        {
            return new List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders>()
            {
                new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders
                {
                    MaxTotalDue = 0,
                    MinTotalDue = 0,
                    TerritoryName = "America"

                }

            };
        }
        public List<AvgSickleaveHoursPerEmployeeMaritalStatus> GetAllMockAvgSickleaveHoursPerEmployeeMaritalStatus()
        {
            return new List<AvgSickleaveHoursPerEmployeeMaritalStatus>()
            {
                new AvgSickleaveHoursPerEmployeeMaritalStatus
                {
                    AverageSickLeaveHours = 1,
                    MaritalStatus = "S"
                }

            };
        }
    }
}

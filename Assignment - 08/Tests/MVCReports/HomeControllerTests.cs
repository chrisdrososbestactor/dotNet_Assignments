﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MVCReports.Controllers;
using MVCReports.Models;

namespace Tests
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        [Description("Checks if the Home Controller is returning any errors while tryingto Retrieve data from the mocked API")]
        public async Task GetAvgSickleaveHoursPerEmployeeMaritalStatusFromController_RuntimeDoesNotGivesExceptions_Test()
        {
            // Arrange
            // Mock Database Creation
            var mockReportsRepository = new Mock<ReportsRepository>().As<IReportsRepository>();
            mockReportsRepository.CallBase = true;
            mockReportsRepository.Setup(x => x.GetAllAvgSickleaveHoursPerEmployeeMaritalStatus("ADO"))
                .Returns(Task.FromResult(new MVCReportsMockDatabaseRecordsProvider()
                .GetAllMockAvgSickleaveHoursPerEmployeeMaritalStatus()));

            // Create mock logger
            var loger = new Logger<HomeController>(new LoggerFactory());

            // Create mock controller
            var testController = new HomeController(loger, mockReportsRepository.Object);
            var result = await testController.AvgSickleaveHoursPerEmployeeMaritalStatus() as ViewResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result != null);
        }
        [TestMethod]
        [Description("Checks if the Home Controller is returning any errors while tryingto Retrieve data from the mocked API")]
        public async Task GetEmployeeListThatLastNameStartsWithLFromController_RuntimeDoesNotGivesExceptions_Test()
        {
            // Arrange
            // Mock Database Creation
            var mockReportsRepository = new Mock<ReportsRepository>().As<IReportsRepository>();
            mockReportsRepository.CallBase = true;
            mockReportsRepository.Setup(x => x.GetAllEmployeeListThatLastNameStartsWithL("ADO"))
                .Returns(Task.FromResult(new MVCReportsMockDatabaseRecordsProvider()
                .GetAllMockEmployeeListThatLastNameStartsWithL()));

            // Create mock logger
            var loger = new Logger<HomeController>(new LoggerFactory());

            // Create mock controller
            var testController = new HomeController(loger, mockReportsRepository.Object);
            var result = await testController.EmployeeListThatLastNameStartsWithL() as ViewResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result != null);
        }
        [TestMethod]
        [Description("Checks if the Home Controller is returning any errors while tryingto Retrieve data from the mocked API")]
        public async Task GetNumEmployeesByGenderFromController_RuntimeDoesNotGivesExceptions_Test()
        {
            // Arrange
            // Mock Database Creation
            var mockReportsRepository = new Mock<ReportsRepository>().As<IReportsRepository>();
            mockReportsRepository.CallBase = true;
            mockReportsRepository.Setup(x => x.GetAllNumEmployeesByGender("ADO"))
                .Returns(Task.FromResult(new MVCReportsMockDatabaseRecordsProvider()
                .GetAllMockNumEmployeesByGenders()));

            // Create mock logger
            var loger = new Logger<HomeController>(new LoggerFactory());

            // Create mock controller
            var testController = new HomeController(loger, mockReportsRepository.Object);
            var result = await testController.NumEmployeesByGender() as ViewResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result != null);
        }
        [TestMethod]
        [Description("Checks if the Home Controller is returning any errors while tryingto Retrieve data from the mocked API")]
        public async Task GetSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersFromController_RuntimeDoesNotGivesExceptions_Test()
        {
            // Arrange
            // Mock Database Creation
            var mockReportsRepository = new Mock<ReportsRepository>().As<IReportsRepository>();
            mockReportsRepository.CallBase = true;
            mockReportsRepository.Setup(x => x.GetAllSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders("ADO"))
                .Returns(Task.FromResult(new MVCReportsMockDatabaseRecordsProvider()
                .GetAllMockSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()));

            // Create mock logger
            var loger = new Logger<HomeController>(new LoggerFactory());

            // Create mock controller
            var testController = new HomeController(loger, mockReportsRepository.Object);
            var result = await testController.SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders() as ViewResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result != null);
        }
        [TestMethod]
        [Description("Unaccepted URL should throw 404 exception")]
        public async Task UnacceptedURLReturnsError404OnUnknownURL_HomeControllerTest()
        {
            HttpResponseMessage result;
            var BaseUrl = "http://localhost:50059";
            var subUrl = "/UnacceptedURL/";
            var resultData = new List<NumEmployeesByGender>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                result = await client.GetAsync(subUrl);
            }
            Assert.IsTrue(result.StatusCode == System.Net.HttpStatusCode.NotFound);
        }
        [TestMethod]
        [Description("Unaccepted Dal parameter value should throw Interval Server Error")]
        public async Task InternalServerErrorReturnedOnIncorrectDalValue_HomeControllerTest()
        {
            HttpResponseMessage result;
            var BaseUrl = "http://localhost:50059";
            var subUrl = "/Reports/NumEmployeesByGender/" + "IncorectInput";
            var resultData = new List<NumEmployeesByGender>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                result = await client.GetAsync(subUrl);
            }

            Assert.IsTrue(result.StatusCode == System.Net.HttpStatusCode.InternalServerError);
        }
    }
}

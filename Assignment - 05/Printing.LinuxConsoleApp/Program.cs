﻿using Printing.Services;
using Printing.UIBase;
using System;

namespace Printing.LinuxConsoleApp
{
    class Program : PrintHelper
    {
        static void Main(string[] args)
        {
            
            var printHelper = new PrintHelper();
            printHelper.PrintProcedure();
        }
    }
}

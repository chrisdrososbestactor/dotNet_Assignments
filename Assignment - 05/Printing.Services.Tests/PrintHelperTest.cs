﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Printing.ConsoleApp;
using Printing.UIBase;

namespace Printing.Tests
{
    [TestClass]
    public class PrintHelperTest
    {

        [TestMethod]
        [Description("Checks if the User has give unacceptable input on the print type selection")]
        public void CheckUserInputTestUnacceptableInput()
        {
            var printHelper = new PrintHelper();
            var input = "something";
            var result = printHelper.CheckUserInput(input);
            Assert.AreEqual(result, 0);
        }
        [TestMethod]
        public void CreateOrderListValidatorTestValidInput()
        {
            var printHelper = new PrintHelper();
            var list = new List<int>();
            list.Add(1);
            var result = printHelper.CreateOrderListValidator(0, list.Count);
            Assert.AreEqual(result, true);
        }
        [TestMethod]
        public void CreateOrderListValidatorTestInvalidInput()
        {
            var printHelper = new PrintHelper();
            var list = new List<int>();

            var result = printHelper.CreateOrderListValidator(0, list.Count);
            Assert.AreEqual(result, false);
        }

        [TestMethod]
        public void CheckUserInputTestAcceptableInput()
        {
            var printHelper = new PrintHelper();
            var input1 = "1";
            var input2 = "2";
            var result1 = printHelper.CheckUserInput(input1);
            var result2 = printHelper.CheckUserInput(input2);
            Assert.IsTrue(result1 != 0 && result2 != 0);
        }

        [TestMethod]
        public void IsUserInputNullTestInputWasNull()
        {
            var printHelper = new PrintHelper();
            var userInput = string.Empty;
            var isUserInputNull = printHelper.IsUserInputNull(userInput);
            Assert.AreEqual(isUserInputNull, true);
        }
        [TestMethod]
        public void IsUserInputNullTestInputWasNotNull()
        {
            var printHelper = new PrintHelper();
            var userInput = "string.NotEmpty";
            var isUserInputNull = printHelper.IsUserInputNull(userInput);
            Assert.AreEqual(isUserInputNull, false);
        }

        [TestMethod]
        [Description("Checking if the function returns -1 if input was not number")]
        public void CheckUserInputForPositiveNumberTestInputWasNotNumber()
        {
            var printHelper = new PrintHelper();
            var userInput = "string.NotEmpty";
            var isUserInputNumber = printHelper.CheckUserInputForPositiveNumber(userInput);
            Assert.IsTrue(isUserInputNumber == -1);
        }
        [TestMethod]
        [Description("Checking if the function returns -1 if input was negative number")]
        public void CheckUserInputForPositiveNumberTestInputWasNegativeNumber()
        {
            var printHelper = new PrintHelper();
            var userInput = "-10";
            var isUserInputNumber = printHelper.CheckUserInputForPositiveNumber(userInput);
            Assert.IsTrue(isUserInputNumber == -1);
        }
        [TestMethod]
        [Description("Checking if the function returns a positive number or 0 if input is >=0")]
        public void CheckUserInputForPositiveNumberTestInputWasPositiveNumber()
        {
            var printHelper = new PrintHelper();
            var userInput = "1";
            var isUserInputNumber = printHelper.CheckUserInputForPositiveNumber(userInput);
            Assert.IsTrue(isUserInputNumber >= 0);
        }
    }
}

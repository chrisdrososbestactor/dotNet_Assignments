﻿using Newtonsoft.Json;
using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Printing.ConsoleApp;
using Printing.DAL_ADO;

namespace Printing.Tests
{
    [TestClass]
    public class DAL_ADOTest
    {
        // Simple test of Data Retrieve and Fetch
        [TestMethod]
        public void FetchAndMapEmpoyeesTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapEmpoyees();

            Assert.IsTrue(testList != null);
        }
        [TestMethod]
        public void FetchAndMapPersonsTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapPersons();

            Assert.IsTrue(testList != null);
        }
        [TestMethod]
        public void FetchAndMapSalesOrderHeadersTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapSalesOrderHeaders();

            Assert.IsTrue(testList != null);
        }
        [TestMethod]
        public void FetchAndMapSalesOrderHeaderSalesReasonsTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapSalesOrderHeaderSalesReasons();

            Assert.IsTrue(testList != null);
        }
        [TestMethod]
        public void FetchAndMapSalesReasonsTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapSalesReasons();

            Assert.IsTrue(testList != null);
        }
        [TestMethod]
        public void FetchAndMapSalesTerritoriesTest()
        {
            var dalCommands = new Commands();
            var testList = dalCommands.FetchAndMapSalesTerritories();

            Assert.IsTrue(testList != null);
        }
    }
}

﻿using Newtonsoft.Json;
using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Printing.ConsoleApp;
using Printing.Services;
using Printing.Models;

namespace Printing.Tests
{
    [TestClass]
    public class PrintFunctionsTest
    {
        [TestMethod]
        public void PrintHelloWorldTest()
        {
            var output = new Printing.Services.PrintFunctions();

            var actual = output.PrintHelloWorld();
            var expected = "Hello World!";

            Assert.AreEqual(actual, expected);
        }
        [TestMethod]
        public void PrintOrderTest()
        {
            var order = new Order();
            var printing = new Printing.Services.PrintFunctions();
            var json = JsonConvert.SerializeObject(order);

            var actual = printing.PrintOrder(order);
            var expected = json;

            Assert.AreEqual(actual, expected);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Printing.Models
{
    public class SalesReason
    {
        public int SalesReasonID { get; set; }
        public string Name { get; set; } = String.Empty;
        public string ReasonType { get; set; } = String.Empty;
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "SalesReasonID: " + SalesReasonID;
        public SalesReason() { }
        public SalesReason(int SalesReasonID_, string Name_, string ReasonType_, DateTime ModifiedDate_)
        {
            this.SalesReasonID = SalesReasonID_;
            this.Name = Name_;
            this.ReasonType = ReasonType_;
            this.ModifiedDate = ModifiedDate_;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Printing.Models
{
    
    public class SalesOrderHeaderSalesReason
    {
        public int SalesOrderID { get; set; }
        public int SalesReasonID { get; set; }
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "SalesOrderID: " + SalesOrderID;
        public  SalesOrderHeaderSalesReason() { }
        public SalesOrderHeaderSalesReason(int SalesOrderID_, int SalesReasonID_, DateTime ModifiedDate_)
        {
            this.SalesOrderID = SalesOrderID_;
            this.SalesReasonID = SalesReasonID_;
            this.ModifiedDate = ModifiedDate_;
        }
    }
   

}

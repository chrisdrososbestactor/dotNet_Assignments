﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Printing.Models
{
    public class Employee
    {
        public int BusinessEntityID { get; set; }
        public string NationalIDNumber { get; set; } = String.Empty;
        public string LoginID { get; set; } = String.Empty;
        public string? OrganizationNode { get; set; }
        public int? OrganizationLevel { get; set; }
        public string JobTitle { get; set; } = String.Empty;
        public DateTime BirthDate { get; set; }
        public string MaritalStatus { get; set; } = String.Empty;
        public string Gender { get; set; } = String.Empty;
        public DateTime HireDate { get; set; }
        public string SalariedFlag { get; set; } = String.Empty;
        public short VacationHours { get; set; }
        public short SickLeaveHours { get; set; }
        public string CurrentFlag { get; set; } = String.Empty;
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
        public override string ToString() => "BusinessEntityID: " + BusinessEntityID;
        public Employee() { }
        public Employee(int OrganizationLevel_,string SalariedFlag_, string CurrentFlag_, int BusinessEntityID_, string NationalIDNumber_, string LoginID_, string? _OrganizationNode, string JobTitle_, DateTime BirthDate_, string MaritalStatus_, string Gender_, DateTime HireDate_, short VacationHours_, short SickLeaveHours_, Guid rowguid_, DateTime ModifiedDate_)
        {
            this.OrganizationLevel = OrganizationLevel_;
            this.SalariedFlag = SalariedFlag_;
            this.CurrentFlag = CurrentFlag_;
            this.BusinessEntityID = BusinessEntityID_;
            this.NationalIDNumber = NationalIDNumber_;
            this.LoginID = LoginID_;
            this.OrganizationNode = _OrganizationNode;
            this.JobTitle = JobTitle_;
            this.BirthDate = BirthDate_;
            this.MaritalStatus = MaritalStatus_;
            this.Gender = Gender_;
            this.HireDate = HireDate_;
            this.VacationHours = VacationHours_;
            this.SickLeaveHours = SickLeaveHours_;
            this.rowguid = rowguid_;
            this.ModifiedDate = ModifiedDate_;
        }
    }

}

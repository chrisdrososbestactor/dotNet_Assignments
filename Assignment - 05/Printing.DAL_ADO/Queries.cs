﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Printing.DAL_ADO
{
    public class Queries
    {
        public string EmployeeQuery()
        {
            return "SELECT [BusinessEntityID]" +
                    ",[NationalIDNumber]" +
                    ",[LoginID]" +
                    ",(select convert(nvarchar(20),convert(varbinary(20),[OrganizationNode],1),1)) as OrganizationNode" +
                    ",[OrganizationLevel]" +
                    ",[JobTitle]" +
                    ",[BirthDate]" +
                    ",[MaritalStatus]" +
                    ",[Gender]" +
                    ",[HireDate]" +
                    ",[SalariedFlag]" +
                    ",[VacationHours]" +
                    ",[SickLeaveHours]" +
                    ",[CurrentFlag]" +
                    ",[rowguid]" +
                    ",[ModifiedDate]" +
                    " FROM[AdventureWorks2017].[HumanResources].[Employee]";
        }

        public string PersonQuery()
        {
            return "SELECT [BusinessEntityID]" +
                    ",[PersonType]" +
                    ",[NameStyle]" +
                    ",[Title]" +
                    ",[FirstName]" +
                    ",[MiddleName]" +
                    ",[LastName]" +
                    ",[Suffix]" +
                    ",[EmailPromotion]" +
                    ",[AdditionalContactInfo]" +
                    ",[Demographics]" +
                    ",[rowguid]" +
                    ",[ModifiedDate]" +
                    "FROM[AdventureWorks2017].[Person].[Person]";
        }
        public string SalesTerritoryQuery()
        {
            return "SELECT TOP (1000) [TerritoryID]"+
            "      ,[Name]"+
            "      ,[CountryRegionCode]"+
            "      ,[Group]"+
            "      ,[SalesYTD]"+
            "      ,[SalesLastYear]"+
            "      ,[CostYTD]"+
            "      ,[CostLastYear]"+
            "      ,[rowguid]"+
            "      ,[ModifiedDate]"+
            "  FROM [AdventureWorks2017].[Sales].[SalesTerritory]";
        }

        public string SalesOrderHeaderSalesReasonQuery()
        {
           return "SELECT  [SalesOrderID]" +
            "      ,[SalesReasonID]" +
            "      ,[ModifiedDate]" +
            "  FROM [AdventureWorks2017].[Sales].[SalesOrderHeaderSalesReason]";
        }
        public string SalesOrderHeaderQuery()
        {
            return "SELECT [SalesOrderID]" +
            "      ,[RevisionNumber]" +
            "      ,[OrderDate]" +
            "      ,[DueDate]" +
            "      ,[ShipDate]" +
            "      ,[Status]" +
            "      ,[OnlineOrderFlag]" +
            "      ,[SalesOrderNumber]" +
            "      ,[PurchaseOrderNumber]" +
            "      ,[AccountNumber]" +
            "      ,[CustomerID]" +
            "      ,[SalesPersonID]" +
            "      ,[TerritoryID]" +
            "      ,[BillToAddressID]" +
            "      ,[ShipToAddressID]" +
            "      ,[ShipMethodID]" +
            "      ,[CreditCardID]" +
            "      ,[CreditCardApprovalCode]" +
            "      ,[CurrencyRateID]" +
            "      ,[SubTotal]" +
            "      ,[TaxAmt]" +
            "      ,[Freight]" +
            "      ,[TotalDue]" +
            "      ,[Comment]" +
            "      ,[rowguid]" +
            "      ,[ModifiedDate]" +
            "  FROM [AdventureWorks2017].[Sales].[SalesOrderHeader]";
        }
        public string SalesReasonQuery()
        {
            return "SELECT [SalesReasonID]"+
                  ",[Name]"+
                  ",[ReasonType]"+
                  ",[ModifiedDate]"+
                  "FROM[AdventureWorks2017].[Sales].[SalesReason];";
        }
    }
}

﻿using Printing.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Printing.DAL_ADO
{
    public class Commands
    {
        public List<Employee> FetchAndMapEmpoyees()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var employeeList = new List<Employee>();
                SqlCommand command = new SqlCommand(queries.EmployeeQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var employee = new Employee()
                        {
                            BusinessEntityID = (int)reader[0],
                            NationalIDNumber = reader[1].ToString(),
                            LoginID = reader[2].ToString(),
                            OrganizationNode = reader[3] as string,
                            OrganizationLevel = reader[4] as int?,
                            JobTitle = reader[5].ToString(),
                            BirthDate = Convert.ToDateTime(reader[6]),
                            MaritalStatus = reader[7].ToString(),
                            Gender = reader[8].ToString(),
                            HireDate = Convert.ToDateTime(reader[9]),
                            SalariedFlag = reader[10].ToString(),
                            VacationHours = (short)reader[11],
                            SickLeaveHours = (short)reader[12],
                            CurrentFlag = reader[13].ToString(),
                            rowguid = (Guid)reader[14],
                            ModifiedDate = Convert.ToDateTime(reader[15])
                        };
                        employeeList.Add(employee);

                    }
                    reader.Close();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return employeeList;
            }
        }
        public List<Person> FetchAndMapPersons()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var personList = new List<Person>();
                SqlCommand command = new SqlCommand(queries.PersonQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var person = new Person()
                        {
                            BusinessEntityID = (int)reader[0],
                            PersonType = reader[1].ToString(),
                            NameStyle = reader[2].ToString(),
                            Title = reader[3] as string,
                            FirstName = reader[4].ToString(),
                            MiddleName = reader[5] as string,
                            LastName = reader[6].ToString(),
                            Suffix = reader[7] as string,
                            EmailPromotion = (int)reader[8],
                            AdditionalContactInfo = reader[9] as string,
                            Demographics = reader[10] as string,
                            rowguid = (Guid)reader[11],
                            ModifiedDate = Convert.ToDateTime(reader[12])
                        };
                        personList.Add(person);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return personList;
            }
        }
        public List<SalesReason> FetchAndMapSalesReasons()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var salesReasonList = new List<SalesReason>();
                SqlCommand command = new SqlCommand(queries.SalesReasonQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var salesReason = new SalesReason()
                        {
                            SalesReasonID = (int)reader[0],
                            Name = reader[1].ToString(),
                            ReasonType = reader[2].ToString(),
                            ModifiedDate = Convert.ToDateTime(reader[3])
                        };
                        salesReasonList.Add(salesReason);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return salesReasonList;
            }
        }
        public List<SalesOrderHeaderSalesReason> FetchAndMapSalesOrderHeaderSalesReasons()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var salesOrderHeaderSalesReasonList = new List<SalesOrderHeaderSalesReason>();
                SqlCommand command = new SqlCommand(queries.SalesOrderHeaderSalesReasonQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var salesOrderHeaderSalesReason = new SalesOrderHeaderSalesReason()
                        {
                            SalesOrderID = (int)reader[0],
                            SalesReasonID = (int)reader[1],
                            ModifiedDate = Convert.ToDateTime(reader[2])
                        };
                        salesOrderHeaderSalesReasonList.Add(salesOrderHeaderSalesReason);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return salesOrderHeaderSalesReasonList;
            }
        }
        public List<SalesTerritory> FetchAndMapSalesTerritories()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var salesTerritoryList = new List<SalesTerritory>();
                SqlCommand command = new SqlCommand(queries.SalesTerritoryQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var salesTerritory = new SalesTerritory()
                        {
                            TerritoryID = (int)reader[0],
                            Name = reader[1].ToString(),
                            CountryRegionCode = reader[2].ToString(),
                            Group = reader[3].ToString(),
                            SalesYTD = (decimal)reader[4],
                            SalesLastYear = (decimal)reader[5],
                            CostYTD = (decimal)reader[6],
                            CostLastYear = (decimal)reader[7],
                            rowguid = (Guid)reader[8],
                            ModifiedDate = Convert.ToDateTime(reader[9])
                        };
                        salesTerritoryList.Add(salesTerritory);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return salesTerritoryList;
            }
        }
        public List<SalesOrderHeader> FetchAndMapSalesOrderHeaders()
        {
            using (SqlConnection connection = new Connection().ConnectToDB())
            {
                var queries = new Queries();
                var salesOrderHeaderList = new List<SalesOrderHeader>();
                SqlCommand command = new SqlCommand(queries.SalesOrderHeaderQuery(), connection);

                // Open the connection in a try/catch block. 
                // Create and execute the DataReader, writing the result
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var salesOrderHeader = new SalesOrderHeader()
                        {
                            SalesOrderID = (int)reader[0],
                            RevisionNumber = (byte)reader[1],
                            OrderDate = Convert.ToDateTime(reader[2]),
                            DueDate = Convert.ToDateTime(reader[3]),
                            ShipDate = Convert.ToDateTime(reader[4]),
                            Status = (byte)reader[5],
                            OnlineOrderFlag = (bool)reader[6],
                            SalesOrderNumber = reader[7].ToString(),
                            PurchaseOrderNumber = reader[8] as string,
                            AccountNumber = reader[9] as string,
                            CustomerID = (int)reader[10],
                            SalesPersonID = reader[11] as int?,
                            TerritoryID = (int)reader[12],
                            BillToAddressID = (int)reader[13],
                            ShipToAddressID = (int)reader[14],
                            ShipMethodID = (int)reader[15],
                            CreditCardID = reader[16] as int?,
                            CreditCardApprovalCode = reader[17] as string,
                            CurrencyRateID = reader[18] as int?,
                            SubTotal = (decimal)reader[19],
                            TaxAmt = (decimal)reader[20],
                            Freight = (decimal)reader[21],
                            TotalDue = (decimal)reader[22],
                            Comment = reader[23] as string,
                            rowguid = (Guid)reader[24],
                            ModifiedDate = Convert.ToDateTime(reader[25])
                        };
                        salesOrderHeaderList.Add(salesOrderHeader);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return salesOrderHeaderList;
            }
        }
    }
}

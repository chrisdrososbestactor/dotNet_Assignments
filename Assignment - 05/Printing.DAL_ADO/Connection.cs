﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security;

namespace Printing.DAL_ADO
{
    public class Connection
    {
        static private string ConnectionString = "Persist Security Info=True;Integrated Security=False;database=AdventureWorks2017;Server=.\\MSSQL2017";
        static private string Username = "sa";
        static private string Password = "12345";
        public bool InTransaction { get; set; }

        public SqlConnection ConnectToDB()
        {
            SecureString sec = new SecureString();
            string pwd = Password;
            pwd.ToCharArray().ToList().ForEach(sec.AppendChar);
            sec.MakeReadOnly();
            return new SqlConnection(ConnectionString, new SqlCredential(Username, sec));
        }
        public SqlConnection ConnectToDB(string connectionString, string username, string password)
        {
            SecureString sec = new SecureString();
            string pwd = password;
            pwd.ToCharArray().ToList().ForEach(sec.AppendChar);
            sec.MakeReadOnly();
            return new SqlConnection(connectionString, new SqlCredential(username, sec));
        }
    }
}

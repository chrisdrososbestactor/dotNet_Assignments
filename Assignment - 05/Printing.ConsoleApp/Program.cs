﻿using Printing.UIBase;


namespace Printing.ConsoleApp
{
    class Program : PrintHelper
    {
        static void Main(string[] args)
        {            
            var printHelper = new PrintHelper();
            printHelper.PrintProcedure();            
        }
    }
}

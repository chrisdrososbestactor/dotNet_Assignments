﻿using Printing.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Printing.DAL_ADO;
using System.Data.SqlClient;
using System.Security;
using System.Net;
using System.Globalization;
using Printing.Models;

namespace Printing.UIBase 
{
    public class PrintHelper
    {
        public void PrintProcedure()
        {
            var userChoice = -1;
            do
            {
                PrintMenu();
                string usrInput = Console.ReadLine();
                userChoice = CheckUserInput(usrInput);
                if (userChoice == 1)
                    PrintTypeHelloWorld();
                else if (userChoice == 2)
                    PrintTypeOrder();
                else if (userChoice == 3)
                    PrintSQLCommands();
                else if (userChoice == 0)
                    ScreenPrint("You didn't entered 1 or 2 or 3, try again.\r\n");
            } while (true);
        }
        public void PrintSQLCommands()
        {
            var dalCommands = new Commands();
            var eployeeList = dalCommands.FetchAndMapEmpoyees();
            ScreenPrint("Employee Table sample");
            if (eployeeList.Count > 8)
            {
                foreach (var item in eployeeList.Take(10))
                    Console.WriteLine(item);
            }
            var personList = dalCommands.FetchAndMapPersons();
            ScreenPrint("Persons Table sample");
            if (personList.Count > 8)
            {
                foreach (var item in personList.Take(10))
                    Console.WriteLine(item);
            }
            var salesReasonsList = dalCommands.FetchAndMapSalesReasons();
            ScreenPrint("SalesReasons Table sample");
            if (salesReasonsList.Count > 8)
            {
                foreach (var item in salesReasonsList.Take(10))
                    Console.WriteLine(item);
            }
            var salesOrderHeaderSalesReasonsList = dalCommands.FetchAndMapSalesOrderHeaderSalesReasons();
            ScreenPrint("SalesOrderHeaderSalesReasons Table sample");
            if (salesOrderHeaderSalesReasonsList.Count > 8)
            {
                foreach (var item in salesOrderHeaderSalesReasonsList.Take(10))
                    Console.WriteLine(item);
            }
            var salesTerritoriesList = dalCommands.FetchAndMapSalesTerritories();
            ScreenPrint("SalesTerritories Table sample");
            if (salesTerritoriesList.Count > 8)
            {
                foreach (var item in salesTerritoriesList.Take(10))
                    Console.WriteLine(item);
            }
            //FetchAndMapSalesOrderHeaders
            var salesOrderHeadersList = dalCommands.FetchAndMapSalesOrderHeaders();
            ScreenPrint("SalesOrderHeaders Table sample");
            if (salesOrderHeadersList.Count > 8)
            {
                foreach (var item in salesOrderHeadersList.Take(10))
                    Console.WriteLine(item);
            }
        }
        
        public void PrintMenu()
        {
            ScreenPrint("Choose Printing type:\r\n" +
                "Enter 1 for Greetings or \r\n" +
                "2 to Print the Order\r\n" +
                "or 3 for printing the first column of the first 10 matches from some tables:\r\n");
        }
        public Order CreateOrder()
        {
            var idCounter = 1;

            var newOrderIDList = new List<int>();
            var listOrderCompleted = false;
            string usrInput = string.Empty;

            // Creating the List with the item ID's
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            int result = -1;
            do
            {
                ScreenPrint("Give 0 as input to stop the OrderList Creation, " +
                    "any input other than a positive number that represends the item ID will be ignored.\r\n" +
                    "Enter the ID of the #" + idCounter + " product:\r\n");
                usrInput = Console.ReadLine();

                result = CheckUserInputForPositiveNumber(usrInput);
                if (result > 0)
                {
                    idCounter++;
                    newOrderIDList.Add(result);
                }
                listOrderCompleted = CreateOrderListValidator(result, newOrderIDList.Count);

            } while (!listOrderCompleted); // stops when input is 0 except when the list is empty
            //newOrder.OrderItems = newOrderIDList;

            // Creating ClientName      
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            var clientName = string.Empty;
            do
            {
                ScreenPrint("Enter Clients Name:(empty input is not accepted)\r\n");
                clientName = Console.ReadLine();
            } while (IsUserInputNull(clientName)); // Repeat until input recieved other than null
            //newOrder.ClientName = clientName;

            // Creating EmployeeName           
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable//
            var empName = string.Empty;
            do
            {
                ScreenPrint("Enter Employee Name:(empty input is not accepted)\r\n");
                empName = Console.ReadLine();
            } while (IsUserInputNull(empName)); // Repeat until input recieved other than null
                                                //newOrder.EmployeeName = empName;

            // Setting DateOrder from Current Date and Time


            // Creating Price
            //TODO: Break this into a seperate function if the client request changes, for now the length is acceptable
            usrInput = string.Empty;
            var price = -1;
            do
            {
                ScreenPrint("Enter total Price:(only numbers >=0 are accepted)\r\n");
                usrInput = Console.ReadLine();
                price = CheckUserInputForPositiveNumber(usrInput);
            } while (price == -1); // Repeat until input was a number >= 0

            //return newOrder.CreateOrder(idCounter);//newOrder;
            var newOrder = new Order
            {
                OrderItems = newOrderIDList,
                ClientName = clientName,
                EmployeeName = empName,
                DateOrdered = DateTime.Now,
                Price = price
            };
            return newOrder;
        }
        public void PrintTypeHelloWorld()
        {
            var printing = new PrintFunctions();
            ScreenPrint(printing.PrintHelloWorld());
        }
        public void PrintTypeOrder()
        {
            var printing = new PrintFunctions();
            Printing.Models.Order newOrder = CreateOrder();
            var printOrder = printing.PrintOrder(newOrder);
            Console.WriteLine(printOrder);
        }
        // Break List creation when the list is not empty and the input is 0
        public bool CreateOrderListValidator(int usrInput, int listCount)
        {
            if (listCount == 0)
                return false;
            else
            {
                if (usrInput == 0)
                    return true;
                else
                    return false;
            }
        }
        public int CheckUserInput(string usrInput)
        {
            var choice = 0;
            var message = string.Empty;
            Int32.TryParse(usrInput, out choice);
            if (choice != 1 && choice != 2 && choice != 3)
                choice = 0;
            return choice;
        }
        public int CheckUserInputForPositiveNumber(string usrInput)
        {
            var choice = -1;
            bool isNum = false;
            isNum = Int32.TryParse(usrInput, out choice);
            if (isNum)
            {
                if (choice < 0)
                    choice = -1;
            }
            else
                choice = -1;
            return choice;
        }
        public bool IsUserInputNull(string usrInput)
        {
            if (usrInput == string.Empty)
                return true;
            else
                return false;
        }
        public void ScreenPrint(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}

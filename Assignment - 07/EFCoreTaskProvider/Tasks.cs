﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interfaces;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EFCoreTaskProvider
{
    /// <summary>
    /// Task provider for EFCore, Using DI with AddDbContextPool
    /// </summary>
    public class Tasks : ITaskEnumerableProvider
    {
        protected string ConnectionString;
        DAL_EFCore.AdventureWorks2017Context CurrentContext;
        public Tasks(DAL_EFCore.AdventureWorks2017Context currentContext)
        {
            CurrentContext = currentContext;
        }
        public Tasks(string connectionString)
        {
            ConnectionString = connectionString;
        }
        DAL_EFCore.AdventureWorks2017Context GetContext()
        {
            if (CurrentContext != null)
                return CurrentContext;
            else
            {
                var serviceCollection = new ServiceCollection()
                    .AddDbContextPool<DAL_EFCore.AdventureWorks2017Context>
                    (
                    options => options.UseSqlServer(ConnectionString)
                    );
                var serviceProvider = serviceCollection.BuildServiceProvider();
                CurrentContext = serviceProvider.GetService<DAL_EFCore.AdventureWorks2017Context>();
                return CurrentContext;
            }
        }
        public IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender()
        {
            return GetContext().Employee
                .GroupBy(e => e.Gender)
                .Select(n => new GetNumEmployeesByGender
                {
                    Gender = n.Key,
                    Count = n.Count()
                })
                .ToList();
        }
        public IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus()
        {
            return GetContext().Employee
                .GroupBy(e => e.MaritalStatus)
                .Select(n => new AvgSickleaveHoursPerEmployeeMaritalStatus
                {
                    MaritalStatus = n.Key,
                    AverageSickLeaveHours = (int)n.Average(x => x.SickLeaveHours)
                })
                .ToList();
        }
        public IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL()
        {
            return GetContext().Person.Join(GetContext().EmailAddress1,
                p => p.BusinessEntityId,
                pea => pea.EmailAddressId,
                (p, pea) => new EmployeeListThatLastNameStartsWithL
                {
                    FirtName = p.FirstName,
                    MiddleName = p.MiddleName,
                    LastName = p.LastName,
                    EmailAddress = pea.EmailAddress,
                    PersonType = p.PersonType
                })
                .Where(f => f.LastName.StartsWith("L"))
                .Where(f => f.PersonType == "EM")
                .OrderBy(x => x.FirtName)
                .ToList();
        }
        public IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
        {
            return (
                from soh in GetContext().SalesOrderHeader
                join sohsr in GetContext().SalesOrderHeaderSalesReason on soh.SalesOrderId equals sohsr.SalesOrderId
                join st in GetContext().SalesTerritory on soh.TerritoryId equals st.TerritoryId
                join sr in GetContext().SalesReason on sohsr.SalesReasonId equals sr.SalesReasonId
                where sr.ReasonType == "Marketing"
                select new
                {
                    st.Name,
                    soh.TotalDue
                })
                .GroupBy(st => new
                {
                    st.Name
                })
                .Select(grouping => new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders
                {
                    TerritoryName = grouping.Key.Name,
                    MinTotalDue = grouping.Min(st => Math.Round(st.TotalDue, 2)),
                    MaxTotalDue = grouping.Max(st => Math.Round(st.TotalDue, 0))
                })
                .OrderBy(x => x.TerritoryName)
                .ToList();
        }
    }
}

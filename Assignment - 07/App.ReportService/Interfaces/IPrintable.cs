﻿using App1.ReportService.Models;

namespace App1.ReportService.Interfaces
{
    public interface IPrintable
    {
        string PrintHelloWorld();
        string PrintOrder(Order order);

    }
}

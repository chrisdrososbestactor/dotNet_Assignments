﻿using App1.ReportService.Interfaces;
using App1.ReportService.Models;
using Newtonsoft.Json;

namespace App1.ReportService
{
    public class PrintFunctions : IPrintable
    {
        public string PrintHelloWorld()
        {
            return "Hello World!";
        }
        public string PrintOrder(Order order)
        {
            return ConvertToJSON(order);
        }
        public string ConvertToJSON(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations.Schema;

namespace DBModels.ADO
{
    [Table("EmailAddress")]
    public class PersonEmailAddress
    {
        public int BusinessEntityID { get; set; }
        public int EmailAddressID { get; set; }
        public string EmailAddress { get; set; } = string.Empty;
        public Guid rowguid { get; set; }
        public DateTime ModifiedDate { get; set; }
    }

}

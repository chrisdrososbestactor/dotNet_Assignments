﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADOTaskProvider.Models;

namespace Interfaces
{
    public interface ITaskQueryableProvider
    {
        IQueryable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IQueryable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IQueryable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IQueryable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class SalesTerritoryRepo : IDataReader<SalesTerritory>
    {
        public IEnumerable<SalesTerritory> GetData(string connectionString)
        {
            var queries = new SalesTerritoryQueries();
            var salesTerritoryList = new List<SalesTerritory>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queries.GetAllFromSalesTerritoryQuery(), connection))
                { 
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var salesTerritory = new SalesTerritory()
                            {
                                TerritoryId = reader.GetInt32(0),
                                Name = reader.GetString(1),
                                CountryRegionCode = reader.GetString(2),
                                Group = reader.GetString(3),
                                SalesYtd = reader.GetDecimal(4),
                                SalesLastYear = reader.GetDecimal(5),
                                CostYtd = reader.GetDecimal(6),
                                CostLastYear = reader.GetDecimal(7),
                                Rowguid = reader.GetGuid(8),
                                ModifiedDate = reader.GetDateTime(9) 
                            };
                            salesTerritoryList.Add(salesTerritory);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return salesTerritoryList;
        }
    }
}

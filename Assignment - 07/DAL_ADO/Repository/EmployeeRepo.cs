﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class EmployeeRepo : IDataReader<Employee>
    {
        public IEnumerable<Employee> GetData(string connectionString)
        {
            var queries = new EmployeeQueries();
            var employeeList = new List<Employee>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {                
                using (SqlCommand command = new SqlCommand(queries.GetAllFromEmployeeQuery(), connection))
                {
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var employee = new Employee()
                            {
                                BusinessEntityId = reader.GetInt32(0),
                                NationalIdnumber = reader.GetString(1),
                                LoginId = reader.GetString(2),
                                OrganizationLevel = reader[4] as short?,
                                JobTitle = reader.GetString(5),
                                BirthDate = reader.GetDateTime(6), 
                                MaritalStatus = reader.GetString(7),
                                Gender = reader.GetString(8),
                                HireDate = reader.GetDateTime(9),
                                SalariedFlag = reader.GetBoolean(10),
                                VacationHours = reader.GetInt16(11),
                                SickLeaveHours = reader.GetInt16(12),
                                CurrentFlag = reader.GetBoolean(13),
                                Rowguid = reader.GetGuid(14), 
                                ModifiedDate = reader.GetDateTime(15)  
                            };
                            employeeList.Add(employee);

                        }
                        reader.Close();

                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return employeeList;
        }        
    }
}

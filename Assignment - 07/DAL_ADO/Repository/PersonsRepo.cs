﻿using DBModels;
using ADO.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DAL_ADO
{
    public class PersonsRepo : IDataReader<Person>
    {
        public IEnumerable<Person> GetData(string connectionString)
        {
            var queries = new PersonQueries();
            var personList = new List<Person>();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {

                using (SqlCommand command = new SqlCommand(queries.GetAllFromPersonQuery(), connection)) { 

                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            var person = new Person()
                            {
                                BusinessEntityId = reader.GetInt32(0),
                                PersonType = reader.GetString(1),
                                NameStyle = reader.GetBoolean(2),
                                Title = reader[3] as string,
                                FirstName = reader.GetString(4),
                                MiddleName = reader[5] as string,
                                LastName = reader.GetString(6),
                                Suffix = reader[7] as string,
                                EmailPromotion = reader.GetInt32(8),
                                AdditionalContactInfo = reader[9] as string,
                                Demographics = reader[10] as string,
                                Rowguid = reader.GetGuid(11),
                                ModifiedDate = reader.GetDateTime(12)
                            };
                            personList.Add(person);
                        }
                        reader.Close();
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }                
            }
            return personList;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class EmployeeQueries
    {
        public string GetAllFromEmployeeQuery()
        {
            return @"SELECT 
                     [BusinessEntityID] 
                    ,[NationalIDNumber] 
                    ,[LoginID] 
                    ,(select convert(nvarchar(20),convert(varbinary(20),[OrganizationNode],1),1)) as OrganizationNode 
                    ,[OrganizationLevel] 
                    ,[JobTitle] 
                    ,[BirthDate] 
                    ,[MaritalStatus] 
                    ,[Gender] 
                    ,[HireDate] 
                    ,[SalariedFlag] 
                    ,[VacationHours] 
                    ,[SickLeaveHours] 
                    ,[CurrentFlag] 
                    ,[rowguid] 
                    ,[ModifiedDate] 
                     FROM [HumanResources].[Employee]";
        }
    }
}

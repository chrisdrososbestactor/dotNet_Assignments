﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class SalesReasonQueries
    {
        public string GetAllFromSalesReasonQuery()
        {
            return @"SELECT 
                   [SalesReasonID]
                  ,[Name]
                  ,[ReasonType]
                  ,[ModifiedDate]
                  FROM [Sales].[SalesReason];";
        }
    }
}

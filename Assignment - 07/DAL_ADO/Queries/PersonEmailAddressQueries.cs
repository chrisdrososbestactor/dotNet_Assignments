﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL_ADO
{
    public class PersonEmailAddressQueries
    {
        public string GetAllFromPersonEmailAddressQuery()
        {
            return @"SELECT 
                     [BusinessEntityID] 
                    ,[EmailAddressID] 
                    ,[EmailAddress] 
                    ,[rowguid] 
                    ,[ModifiedDate] 
                    FROM [Person].[EmailAddress]";
        }
    }
}

﻿using App3.Models;

namespace App3.ReportService.Interfaces
{
    public interface IPrintable
    {
        string PrintHelloWorld();
        string PrintOrder(Order order);

    }
}

﻿namespace App3.Models
{
    /// <summary>
    /// Sales Order Minimum and Maximum total due per territory for marketing related orders (sales reason)
    /// </summary>
    public class SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto
    {
        public string TerritoryName { get; set; }
        public decimal MinTotalDue { get; set; }
        public decimal MaxTotalDue { get; set; }
    }
}

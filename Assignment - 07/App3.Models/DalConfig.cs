﻿using System;

namespace App3.Models
{
    public class DalConfig
    {
        // All the configuration parameter for the connection with the server goes here
        public string DAL { get; set; } = String.Empty;
        public string ConnectionString { get; set; } = String.Empty;

    }
}

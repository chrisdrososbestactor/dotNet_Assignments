﻿using DAL_ADO;
using System.Collections.Generic;
using DBModels;

namespace ADOTaskProvider
{
    /// <summary>
    /// Returns the data to ADOTaskPriver.Tasks for every table is requested
    /// </summary>
    public class GetDataProvider : IADODataProvider
    {
        protected string ConnectionString;
        public GetDataProvider(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public IEnumerable<Employee> GetAllEmployees()
        {
            return new EmployeeRepo().GetData(ConnectionString);
        }
        public IEnumerable<Person> GetAllPersons()
        {
            return new PersonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<EmailAddress1> GetAllPersonEmailAdresses()
        {
            return new PersonEmailAddressesRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesOrderHeader> GetAllSalesOrderHeaders()
        {
            return new SalesOrderHeaderRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesOrderHeaderSalesReason> GetAllSaleOrderHeaderSalesReasons()
        {
            return new SalesOrderHeaderSalesReasonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesReason> GetAllSalesReasons()
        {
            return new SalesReasonsRepo().GetData(ConnectionString);
        }
        public IEnumerable<SalesTerritory> GetAllSalesTerritories()
        {
            return new SalesTerritoryRepo().GetData(ConnectionString);
        }
    }
}

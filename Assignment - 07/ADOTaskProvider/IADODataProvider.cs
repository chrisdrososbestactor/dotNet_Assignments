﻿using System;
using System.Collections.Generic;
using System.Text;
using DBModels;

namespace ADOTaskProvider
{
    public interface IADODataProvider
    {
        IEnumerable<Employee> GetAllEmployees();
        IEnumerable<Person> GetAllPersons();
        IEnumerable<EmailAddress1> GetAllPersonEmailAdresses();
        IEnumerable<SalesOrderHeader> GetAllSalesOrderHeaders();
        IEnumerable<SalesOrderHeaderSalesReason> GetAllSaleOrderHeaderSalesReasons();
        IEnumerable<SalesReason> GetAllSalesReasons();
        IEnumerable<SalesTerritory> GetAllSalesTerritories();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Interfaces;
using DAL.Models;
using DBModels;

namespace ADOTaskProvider
{
    /// <summary>
    /// Task provider for ADO
    /// </summary>
    public class Tasks : ITaskEnumerableProvider
    {
        protected string ConnectionString;
        readonly IADODataProvider GetDataProvider;
        public Tasks(IADODataProvider getDataProvider)
        {
            GetDataProvider = getDataProvider;
        }
        public Tasks(string connectionString)
        {
            GetDataProvider = new GetDataProvider(connectionString);
        }
        IEnumerable<Employee> GetAllEmployees()
        {
            return GetDataProvider.GetAllEmployees();
        }
        IEnumerable<Person> GetAllPersons()
        {
            return GetDataProvider.GetAllPersons();
        }
        IEnumerable<EmailAddress1> GetAllPersonEmailAdresses()
        {
            return GetDataProvider.GetAllPersonEmailAdresses();
        }
        IEnumerable<SalesOrderHeader> GetAllSalesOrderHeaders()
        {
            return GetDataProvider.GetAllSalesOrderHeaders();
        }
        IEnumerable<SalesOrderHeaderSalesReason> GetAllSaleOrderHeaderSalesReasons()
        {
            return GetDataProvider.GetAllSaleOrderHeaderSalesReasons();
        }
        IEnumerable<SalesReason> GetAllSalesReasons()
        {
            return GetDataProvider.GetAllSalesReasons();
        }
        IEnumerable<SalesTerritory> GetAllSalesTerritories()
        {
            return GetDataProvider.GetAllSalesTerritories();
        }
        public IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender()
        {
            return GetAllEmployees()
                .GroupBy(e => e.Gender)
                .Select(n => new GetNumEmployeesByGender
                {
                    Gender = n.Key,
                    Count = n.Count()
                });
        }
        public IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus()
        {
            return GetAllEmployees()
                .GroupBy(e => e.MaritalStatus)
                .Select(n => new AvgSickleaveHoursPerEmployeeMaritalStatus
                {
                    MaritalStatus = n.Key,
                    AverageSickLeaveHours = (int)n.Average(x => x.SickLeaveHours)
                });
        }
        public IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL()
        {
            var personData = GetAllPersons();
            var personEmailAddressData = GetAllPersonEmailAdresses();
            return personData.Join(personEmailAddressData,
                p => p.BusinessEntityId,
                pea => pea.EmailAddressId,
                (p, pea) => new EmployeeListThatLastNameStartsWithL
                {
                    FirtName = p.FirstName,
                    MiddleName = p.MiddleName,
                    LastName = p.LastName,
                    EmailAddress = pea.EmailAddress,
                    PersonType = p.PersonType
                })
                .Where(f => f.LastName.StartsWith("L"))
                .Where(f => f.PersonType == "EM")
                .OrderBy(x => x.FirtName);
        }
        public IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders()
        {
            var salesOrderHeaderData = GetAllSalesOrderHeaders();
            var salesOrderHeaderSalesReasonData = GetAllSaleOrderHeaderSalesReasons();
            var salesReasonData = GetAllSalesReasons();
            var salesTerritoryData = GetAllSalesTerritories();
            return (
                from soh in salesOrderHeaderData
                join sohsr in salesOrderHeaderSalesReasonData on soh.SalesOrderId equals sohsr.SalesOrderId
                join st in salesTerritoryData on soh.TerritoryId equals st.TerritoryId
                join sr in salesReasonData on sohsr.SalesReasonId equals sr.SalesReasonId
                where sr.ReasonType == "Marketing"
                select new
                {
                    st.Name,
                    soh.TotalDue
                })
                .GroupBy(st => new
                {
                    st.Name
                })
                .Select(grouping => new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders
                {
                    TerritoryName = grouping.Key.Name,
                    MinTotalDue = grouping.Min(st => Math.Round(st.TotalDue, 2)),
                    MaxTotalDue = grouping.Max(st => Math.Round(st.TotalDue, 0))
                })
                .OrderBy(x => x.TerritoryName);
        }
    }
}

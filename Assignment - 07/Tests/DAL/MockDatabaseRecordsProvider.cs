﻿using System;
using System.Collections.Generic;
using App3.Models;
using DBModels;

namespace Tests
{
    /// <summary>
    /// Returns Mock tables with 1 record in each one
    /// </summary>
    public class MockDatabaseRecordsProvider
    {
        public List<EmployeeListThatLastNameStartsWithLDto> GetAllMockEmployeeListThatLastNameStartsWithLDtos()
        {
            return new List<EmployeeListThatLastNameStartsWithLDto>()
            {
                new EmployeeListThatLastNameStartsWithLDto
                {
                    EmailAddress = "fake@email.gr",
                    FirtName = "L",
                    LastName = "Lakis",
                    MiddleName = "M",
                    PersonType = "EM"
                }

            };
        }
        public List<NumEmployeesByGenderDto> GetAllMockNumEmployeesByGenderDtos()
        {
            return new List<NumEmployeesByGenderDto>()
            {
                new NumEmployeesByGenderDto
                {
                    Count = 0,
                    Gender = "M"
                }

            };
        }
        public List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto> GetAllMockSalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDtos()
        {
            return new List<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto>()
            {
                new SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrdersDto
                {
                    MaxTotalDue = 0,
                    MinTotalDue = 0,
                    TerritoryName = "America"

                }

            };
        }
        public List<AvgSickleaveHoursPerEmployeeMaritalStatusDto> GetAllMockAvgSickleaveHoursPerEmployeeMaritalStatusDtos()
        {
            return new List<AvgSickleaveHoursPerEmployeeMaritalStatusDto>()
            {
                new AvgSickleaveHoursPerEmployeeMaritalStatusDto
                {
                    AverageSickLeaveHours = 1,
                    MaritalStatus = "S"
                }

            };
        }
        public List<Person> GetAllMockPersons()
        {
            return new List<Person>(){
                new Person
                {
                    BusinessEntityId = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                    PersonType = "EM",
                    NameStyle = false,
                    Title = "Hacker",
                    FirstName = "John",
                    MiddleName = "Doeish",
                    LastName = "Lamborgini",
                    Suffix = "S",
                    EmailPromotion = 1,
                    AdditionalContactInfo = "None",
                    Demographics = "None",
                }
            };
        }
        public List<Employee> GetAllMockEmployees()
        {
            return new List<Employee>()
            {
                new Employee
                {
                    OrganizationLevel = 1,
                    SalariedFlag = false,
                    CurrentFlag = false,
                    BusinessEntityId = 1,
                    NationalIdnumber = "GR",
                    LoginId = "login",
                    JobTitle = "hacker",
                    BirthDate = new DateTime(1983, 7, 7),
                    MaritalStatus = "M",
                    Gender = "M",
                    HireDate = new DateTime(2003, 7, 7),
                    VacationHours = 1,
                    SickLeaveHours = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
        public List<EmailAddress1> GetAllMockPersonEmailAddresses()
        {
            return new List<EmailAddress1>()
            {
                new EmailAddress1
                {
                    BusinessEntityId = 1,
                    EmailAddressId = 1,
                    EmailAddress = "no@email.man",
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
        public List<SalesReason> GetAllMockSalesReasons()
        {
            return new List<SalesReason>()
            {
                new SalesReason
                {
                    Name = "Reason",
                    SalesReasonId = 1,
                    ReasonType = "Marketing",
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
        public List<SalesTerritory> GetAllMockSalesTerritories()
        {
            return new List<SalesTerritory>()
            {
                new SalesTerritory
                {
                    TerritoryId = 1,
                    Name = "DrosoArea",
                    CountryRegionCode = "GR",
                    Group = "EU",
                    SalesYtd = 1,
                    SalesLastYear = 1,
                    CostYtd = 1,
                    CostLastYear = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
        public List<SalesOrderHeader> GetAllMockSalesOrderHeaders()
        {
            return new List<SalesOrderHeader>()
            {
                new SalesOrderHeader
                {
                    SalesOrderId = 1,
                    RevisionNumber = 17,
                    OrderDate = new DateTime(2018, 7, 7),
                    DueDate = new DateTime(2018, 7, 7),
                    ShipDate = new DateTime(2018, 7, 7),
                    Status = 0,
                    OnlineOrderFlag = true,
                    SalesOrderNumber = "1",
                    PurchaseOrderNumber = "1",
                    AccountNumber = "1",
                    CustomerId = 1,
                    SalesPersonId = 1,
                    TerritoryId = 1,
                    BillToAddressId = 1,
                    ShipToAddressId = 1,
                    ShipMethodId = 1,
                    CreditCardId = 1,
                    CreditCardApprovalCode = "zZz",
                    CurrencyRateId = 1,
                    SubTotal = 1,
                    TaxAmt = 1,
                    Freight = 1,
                    TotalDue = 10,
                    Comment = "Bad Client, eliminate",
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
        public List<SalesOrderHeaderSalesReason> GetAllMockSalesOrderHeaderSalesReasons()
        {
            return new List<SalesOrderHeaderSalesReason>()
            {
                new SalesOrderHeaderSalesReason
                {
                    SalesOrderId = 1,
                    SalesReasonId = 1,
                    ModifiedDate = new DateTime(2018, 7, 7),
                }
            };
        }
    }
}

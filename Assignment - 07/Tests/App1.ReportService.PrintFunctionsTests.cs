﻿using Newtonsoft.Json;
using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using App1;
using App1.ReportService;
using App1.ReportService.Models;

namespace Tests
{
    [TestClass]
    public class PrintFunctionsTests
    {
        [TestMethod]
        public void PrintHelloWorldTest()
        {
            var output = new PrintFunctions();

            var actual = output.PrintHelloWorld();
            var expected = "Hello World!";

            Assert.AreEqual(actual, expected);
        }
        [TestMethod]
        public void PrintOrderTest()
        {
            var order = new Order();
            var printing = new PrintFunctions();
            var json = JsonConvert.SerializeObject(order);

            var actual = printing.PrintOrder(order);
            var expected = json;

            Assert.AreEqual(actual, expected);
        }
    }
}

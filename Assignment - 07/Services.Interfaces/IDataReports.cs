﻿using System.Collections.Generic;
using CoreReportService.Models;

namespace CoreReportService.Interfaces
{
    public interface IDataReports
    {
        IEnumerable<SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders> SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders();
        IEnumerable<AvgSickleaveHoursPerEmployeeMaritalStatus> AvgSickleaveHoursPerEmployeeMaritalStatus();
        IEnumerable<EmployeeListThatLastNameStartsWithL> EmployeeListThatLastNameStartsWithL();
        IEnumerable<GetNumEmployeesByGender> GetNumEmployeesByGender();
    }
}

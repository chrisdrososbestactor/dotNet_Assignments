﻿using DAL.Interfaces;

namespace CoreReportService.Interfaces
{
    public interface ITaskProviderFactory
    {
        ITaskEnumerableProvider GetTaskProvider();
    }
}

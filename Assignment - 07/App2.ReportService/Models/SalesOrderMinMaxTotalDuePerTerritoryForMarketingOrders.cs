﻿using System;

namespace App2.ReportService.Models
{
    public class SalesOrderMinMaxTotalDuePerTerritoryForMarketingOrders
    {
        public string TerritoryName { get; set; }
        public decimal MinTotalDue { get; set; }
        public decimal MaxTotalDue { get; set; }
    }
}

﻿namespace LegacyCustomerDirectory.DAL.EF
{
    public interface IContextFactory
    {
        AdventureWorks2017Context GetContext();
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace LegacyCustomerDirectory.DAL.EF
{
    /// <summary>
    /// Context Factory
    /// </summary>
    public class ContextFactory : IContextFactory
    {
        readonly string ConnectionString;
        public ContextFactory(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public AdventureWorks2017Context GetContext()
        {
            var serviceCollection = new ServiceCollection()
                    .AddDbContextPool<AdventureWorks2017Context>
                    (
                    options => options.UseSqlServer(ConnectionString)
                    );
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var currentContext = serviceProvider.GetService<AdventureWorks2017Context>();
            return currentContext;
        }
    }
}

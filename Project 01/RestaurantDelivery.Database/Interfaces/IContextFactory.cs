﻿namespace RestaurantDelivery.DAL.EF.Interfaces

{
    public interface IContextFactory
    {
        RestaurantDeliveryContext GetContext();
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantDelivery.DAL.EF
{
    public class RestaurantDeliveryContextFactory : IDesignTimeDbContextFactory<RestaurantDeliveryContext>
    {
        readonly string ConnectionString = "Trusted_Connection=True;database=RestaurantDelivery;Server=localhost\\MSSQL2017";
        
        public RestaurantDeliveryContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<RestaurantDeliveryContext>();
            optionsBuilder.UseSqlServer(ConnectionString);

            return new RestaurantDeliveryContext(optionsBuilder.Options);
        }
    }
}

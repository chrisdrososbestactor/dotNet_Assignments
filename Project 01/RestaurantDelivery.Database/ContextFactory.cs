﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using RestaurantDelivery.DAL.EF.Interfaces;

namespace RestaurantDelivery.DAL.EF
{
    /// <summary>
    /// Context Factory
    /// </summary>
    public class ContextFactory : IContextFactory
    {
        readonly string ConnectionString;
        public ContextFactory(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public RestaurantDeliveryContext GetContext()
        {
            var serviceCollection = new ServiceCollection()
                    .AddDbContextPool<RestaurantDeliveryContext>
                    (
                    options => options.UseSqlServer(ConnectionString)
                    );
            var serviceProvider = serviceCollection.BuildServiceProvider();
            var currentContext = serviceProvider.GetService<RestaurantDeliveryContext>();
            return currentContext;
        }
    }
}

﻿using System;

namespace RestaurantDelivery.DAL.EF.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class Order
    {
        public int Id { get; set; }
        public DateTime DateOrdered { get; set; }
        public int OrderPrice { get; set; }
        public virtual Product Product { get; set; }

        public virtual Customer Customer { get; set; }
    }
}

﻿namespace RestaurantDelivery.DAL.EF.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int ProductPrice { get; set; }
        public ProductCategory ProductCategory { get; set; }
    }
}

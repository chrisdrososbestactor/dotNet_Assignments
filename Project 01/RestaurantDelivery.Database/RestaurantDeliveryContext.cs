﻿using Microsoft.EntityFrameworkCore;
using RestaurantDelivery.DAL.EF.Models;

namespace RestaurantDelivery.DAL.EF
{
    public partial class RestaurantDeliveryContext : DbContext
    {
        //readonly string ConnectionString;
        public RestaurantDeliveryContext(DbContextOptions<RestaurantDeliveryContext> options)
            : base(options)
        {
        }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductCategory> ProductCategory { get; set; }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    if (!optionsBuilder.IsConfigured)
        //    {
        //        optionsBuilder.UseSqlServer(ConnectionString);
        //    }
        //}
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using RestaurantDelivery.DAL.EF.Interfaces;
using RestaurantDelivery.Service;
using RestaurantDelivery.Service.Models;

namespace Tests
{
    [TestClass]
    public class RestaurantDeliveryDALEFTests
    {
        [TestMethod]
        [Description("Checks the Convertion from SearchTerms object To QueryString string")]
        public void ConvertSearchTermsToQueryString_Test()
        {
            // Arrange
            // Setup terms and CustomerRepo
            var terms = new SearchTerms()
            {
                FirstName = "tripas",
                LastName = "mitsos"
            };
            var mockCustomerRepo = new Mock<CustomerRepo>(It.IsAny<IContextFactory>());

            // Act
            var actual = mockCustomerRepo.Object.ConvertSearchTermsToQueryString(terms);

            //Assert
            Assert.AreEqual(actual, "?FirstName=tripas&LastName=mitsos");
        }
    }
}


﻿using LegacyCustomerDirectory.SearchResultsService;
using LegacyCustomerDirectory.SearchResultsService.Interfaces;
using LegacyCustomerDirectory.SearchResultsService.Models;
using LegacyCustomerDirectoryWebAPI.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass]
    public class SearchResultsControllerTests
    {
        [TestMethod]
        [Description("Checks if the SearchResults Controller is generating any data from the Report")]
        public async Task GetAllOldCustomersContainingTerm_FromSearchResultsControllerTest()
        {
            // Create mock configuration files for every class
            DalConfig config = new LegacyCustomerDirectory.SearchResultsService.Models.DalConfig()
            {
                ConnectionString = "Trusted_Connection=True;database=AdventureWorks2017;Server=localhost\\MSSQL2017",
            };

            // Create mock options, PrintService and logger
            var mockOptions = new Mock<IOptions<DalConfig>>();
            mockOptions.Setup(op => op.Value).Returns(config);
            var searchResultFunctions = new SearchResultFunctions();
            var logger = new Logger<SearchResultsController>(new LoggerFactory());

            // Mock SearchResultServices
            var terms = new LegacyCustomerDirectory.SearchResultsService.Models.SearchTerms()
            {
                FirstName = "John",
                PostalCode = "12345"
            };
            var mockSearchResultServices = new Mock<SearchResultService>().As<ISearchResultService>();
            mockSearchResultServices
                .Setup(x => x.GetAllOldCustomersContainingTermAsync(It.IsAny<DalConfig>(), It.IsAny<SearchTerms>()))
                .ReturnsAsync(new WebApiMockDatabaseRecordsProvider().GetAllMockOldCustomersDtos());

            // Create mock controller
            var testController = new SearchResultsController(logger, mockSearchResultServices.Object, searchResultFunctions, mockOptions.Object);

            // var result = request.Object.Request.Query as ActionResult; 
            var result = await testController.GetAllOldCustomersContainingTermAsync() as OkObjectResult;

            // Check if data is being returned from the Controller
            Assert.IsTrue(result.Value != null);
        }
    }
}

﻿using LegacyCustomerDirectory.SearchResultsService.Models;
using System.Collections.Generic;

namespace Tests
{
    /// <summary>
    /// Returns Mock tables with 1 record in each one
    /// </summary>
    public class WebApiMockDatabaseRecordsProvider
    {
        public List<OldCustomerDto> GetAllMockOldCustomersDtos()
        {
            return new List<OldCustomerDto>()
            {
                new OldCustomerDto
                {
                    EmailAddress = "fake@email.gr",
                    FirstName = "L",
                    LastName = "Lakis",
                    AddressLine1 = "Hollywood, Church of Athens",
                    AddressLine2 = string.Empty,
                    Age = 20,
                    BusinessEntityId = 1,
                    City = "Athens",
                    Gender = "M",
                    JobTitle = "Hacker",
                    MaritalStatus = "M",
                    HomePhoneNumber = "00030004680",
                    WorkPhoneNumber = "00030004680",
                    PostalCode = "12345",
                }

            };
        }
    }
}

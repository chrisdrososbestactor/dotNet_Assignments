﻿using LegacyCustomerDirectory.CustomersProvider;
using LegacyCustomerDirectory.CustomersProvider.Models;
using LegacyCustomerDirectory.DAL.EF;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OldCustomers.Database;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass]
    public class EFCoreDataReportsTests
    {
        void InsertData(DbContextOptions<AdventureWorks2017Context> options)
        {
            using (var context = new AdventureWorks2017Context(options))
            {
                var mockDB = new MockDatabaseRecordsProvider().GetAllMockCustomers();
                context.Person.Add(new Person
                {
                    BusinessEntityId = mockDB[0].BusinessEntityId,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                    PersonType = "EM",
                    NameStyle = false,
                    Title = "Hacker",
                    FirstName = mockDB[0].FirstName,
                    MiddleName = "Doeish",
                    LastName = mockDB[0].LastName,
                    Suffix = "S",
                    EmailPromotion = 1,
                    AdditionalContactInfo = "None",
                    Demographics = "None",
                });
                context.EmailAddress1.Add(new EmailAddress1
                {
                    BusinessEntityId = mockDB[0].BusinessEntityId,
                    EmailAddressId = 1,
                    EmailAddress = mockDB[0].EmailAddress,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.PersonPhone.Add(new PersonPhone
                {
                    BusinessEntityId = mockDB[0].BusinessEntityId,
                    PhoneNumber = mockDB[0].PhoneNumber,
                    PhoneNumberTypeId = mockDB[0].PhoneNumberTypeId,
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.BusinessEntityAddress.Add(new BusinessEntityAddress
                {
                    BusinessEntityId = mockDB[0].BusinessEntityId,
                    AddressId = 1,
                    AddressTypeId = mockDB[0].AddressTypeId,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.Address.Add(new Address
                {
                    AddressId = 1,
                    AddressLine1 = mockDB[0].AddressLine1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                    City = mockDB[0].City,
                    PostalCode = mockDB[0].PostalCode,
                });
                context.Employee.Add(new Employee
                {
                    OrganizationLevel = 1,
                    SalariedFlag = false,
                    CurrentFlag = false,
                    BusinessEntityId = mockDB[0].BusinessEntityId,
                    NationalIdnumber = "GR",
                    LoginId = "login",
                    JobTitle = mockDB[0].JobTitle,
                    BirthDate = mockDB[0].BirthDate,
                    MaritalStatus = mockDB[0].MaritalStatus,
                    Gender = mockDB[0].Gender,
                    HireDate = new DateTime(2003, 7, 7),
                    VacationHours = 1,
                    SickLeaveHours = 1,
                    Rowguid = new Guid(),
                    ModifiedDate = new DateTime(2018, 7, 7),
                });
                context.SaveChanges();
            }
        }
        [TestMethod]
        [Description("Database is being replaced with Mock database. Test check if GetAllOldCustomersContainingTerm find all the customers we expect")]
        public async Task GetAllOldCustomersContainingTerm_Test()
        {
            // Arrange
            // Setup Mock Data and context
            var options = new DbContextOptionsBuilder<AdventureWorks2017Context>()
                .UseInMemoryDatabase(databaseName: "GetAllOldCustomersContainingTerm")
                .Options;
            using (var context = new AdventureWorks2017Context(options))
            {
                InsertData(options);
            }
            using (var context = new AdventureWorks2017Context(options))
            {
                // Arange
                // Mock ContextFactory configured to use in-memory context
                var mockContextFactory = Mock.Of<IContextFactory>(temp =>
                    temp.GetContext() == context);

                // New customer provider with the mocked ConextFactory
                var customerProvider = new CustomerProvider(mockContextFactory);// Mock<CustomerProvider>(string.Empty).As<ICustomerProvider>();

                // Terms to test. In MockDatabase, John exist on FirstName and 12345 on PostalCode
                var terms = new SearchTerms()
                {
                    FirstName = "John",
                    PostalCode = "12345"
                };

                //Act
                var result = await customerProvider.GetAllOldCustomersContainingTermAsync(terms);

                //Assert
                Assert.IsTrue(result.ToList().Count == 1);
            }

        }
    }
}


﻿
using LegacyCustomerDirectory.CustomersProvider.Models;
using System;
using System.Collections.Generic;

namespace Tests
{
    /// <summary>
    /// Returns Mock table with 1 record
    /// </summary>
    public class MockDatabaseRecordsProvider
    {
        public List<OldCustomer> GetAllMockCustomers()
        {
            return new List<OldCustomer>()
            {
                new OldCustomer
                {
                    AddressLine1 = "Hollywood, Church of Athens",
                    AddressLine2 = string.Empty,
                    BirthDate = new DateTime(2000, 7, 7),
                    BusinessEntityId = 1,
                    City = "Athens",
                    FirstName = "John",
                    Gender = "M",
                    JobTitle = "Hacker",
                    LastName = "Snow",
                    MaritalStatus = "M",
                    PhoneNumber = "00030004680",
                    PhoneNumberTypeId = 2,
                    EmailAddress = "fake@email.gr",
                    AddressTypeId = 2,
                    PostalCode = "12345",
                }
            };
        }
    }
}

﻿using LegacyCustomerDirectory.CustomersProvider.Interfaces;
using LegacyCustomerDirectory.CustomersProvider.Models;
using LegacyCustomerDirectory.DAL.EF;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LegacyCustomerDirectory.CustomersProvider
{
    /// <summary>
    /// Results provider for EFCore, Using DI with AddDbContextPool
    /// </summary>
    public class CustomerProvider : ICustomerProvider
    {
        IContextFactory ContextFactory;
        AdventureWorks2017Context CurrentContext;
        public CustomerProvider(IContextFactory contextFactory)
        {
            ContextFactory = contextFactory;
        }
        public AdventureWorks2017Context GetContext()
        {
            if (CurrentContext != null)
                return CurrentContext;
            else
                return CurrentContext = ContextFactory.GetContext();
        }
        /// <summary>
        /// Returns all the Customers that matches any string field with any of the terms
        /// Only Home address, Work phone number and Home phone number are requested.
        /// </summary>
        /// <param name="terms">Array of search terms</param>
        /// <returns>List of Customers</returns>
        public async Task<IEnumerable<OldCustomer>> GetAllOldCustomersContainingTermAsync(SearchTerms terms)
        {
            var query = (
                from p in GetContext().Person
                join ea in GetContext().EmailAddress1 on p.BusinessEntityId equals ea.BusinessEntityId
                join e in GetContext().Employee on p.BusinessEntityId equals e.BusinessEntityId
                join pp in GetContext().PersonPhone on p.BusinessEntityId equals pp.BusinessEntityId
                join bea in GetContext().BusinessEntityAddress on p.BusinessEntityId equals bea.BusinessEntityId
                join a in GetContext().Address on bea.AddressId equals a.AddressId
                select new OldCustomer
                {
                    AddressLine1 = a.AddressLine1,
                    AddressLine2 = a.AddressLine2,
                    BirthDate = e.BirthDate,
                    BusinessEntityId = p.BusinessEntityId,
                    City = a.City,
                    EmailAddress = ea.EmailAddress,
                    FirstName = p.FirstName,
                    Gender = e.Gender,
                    JobTitle = e.JobTitle,
                    LastName = p.LastName,
                    MaritalStatus = e.MaritalStatus,
                    PhoneNumber = pp.PhoneNumber,
                    PhoneNumberTypeId = pp.PhoneNumberTypeId,
                    PostalCode = a.PostalCode,
                    AddressTypeId = bea.AddressTypeId
                });
            query = SearchForTerms(query, terms);
            return await query.ToListAsync();
        }
        /// <summary>
        /// Searches for every Customer that match any of the available term.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        private IQueryable<OldCustomer> SearchForTerms(IQueryable<OldCustomer> query, SearchTerms terms)
        {
            if (!string.IsNullOrEmpty(terms.FirstName))
                query = query.Where(x => x.FirstName.Contains(terms.FirstName));
            if (!string.IsNullOrEmpty(terms.LastName))
                query = query.Where(x => x.LastName.Contains(terms.LastName));
            if (!string.IsNullOrEmpty(terms.Gender))
                query = query.Where(x => x.Gender.Contains(terms.Gender));
            if (!string.IsNullOrEmpty(terms.EmailAddress))
                query = query.Where(x => x.EmailAddress.Contains(terms.EmailAddress));
            if (!string.IsNullOrEmpty(terms.City))
                query = query.Where(x => x.City.Contains(terms.City));
            if (!string.IsNullOrEmpty(terms.JobTitle))
                query = query.Where(x => x.JobTitle.Contains(terms.JobTitle));
            if (!string.IsNullOrEmpty(terms.PhoneNumber))
                query = query.Where(x => x.PhoneNumber.Contains(terms.PhoneNumber));
            if (!string.IsNullOrEmpty(terms.AddressLine1))
                query = query.Where(x => x.AddressLine1.Contains(terms.AddressLine1));
            if (!string.IsNullOrEmpty(terms.AddressLine2))
                query = query.Where(x => x.AddressLine2.Contains(terms.AddressLine2));
            if (!string.IsNullOrEmpty(terms.PostalCode))
                query = query.Where(x => x.PostalCode.Contains(terms.PostalCode));
            return query;

        }

        /// <summary>
        /// This beautifull function don't want me to use it, but i will!!
        /// </summary>
        /// <param name="query">the IQeryable item</param>
        /// <param name="terms">A List of strings for terms</param>
        /// <returns></returns>
        private IQueryable<OldCustomer> QuickSearchOnEveryFieldWithEveryTerm(IQueryable<OldCustomer> query, List<string> terms)
        {
            foreach (string term in terms)
            {
                query = query.Where(
                    x => x.FirstName.Contains(term) ||
                    x.LastName.Contains(term) ||
                    x.EmailAddress.Contains(term) ||
                    x.Gender.Contains(term) ||
                    x.AddressLine1.Contains(term) ||
                    x.AddressLine2.Contains(term) ||
                    x.City.Contains(term) ||
                    x.JobTitle.Contains(term) ||
                    x.PhoneNumber.Contains(term) ||
                    x.PostalCode.Contains(term)
                );
            }
            return query;
        }
    }
}

﻿using System;

namespace LegacyCustomerDirectory.CustomersProvider.Models
{
    /// <summary>
    /// OldCustomer Model. Contains all the information available for a Customer
    /// </summary>
    public class OldCustomer
    {
        public int BusinessEntityId { get; set; }
        public int AddressTypeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public DateTime BirthDate { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneNumberTypeId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}

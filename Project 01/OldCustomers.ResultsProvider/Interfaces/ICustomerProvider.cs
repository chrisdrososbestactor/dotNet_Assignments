﻿using LegacyCustomerDirectory.CustomersProvider.Models;
using LegacyCustomerDirectory.DAL.EF;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LegacyCustomerDirectory.CustomersProvider.Interfaces
{
    public interface ICustomerProvider
    {
        Task<IEnumerable<OldCustomer>> GetAllOldCustomersContainingTermAsync(SearchTerms term);
        AdventureWorks2017Context GetContext();
    }
}
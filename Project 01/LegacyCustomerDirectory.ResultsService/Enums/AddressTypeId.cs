﻿namespace LegacyCustomerDirectory.SearchResultsService.Enums
{
    /// <summary>
    /// OldCustomer Model. Contains all the information available for a Customer
    /// </summary>
    public enum AddressTypeId
    {
        Home = 2
    }
}

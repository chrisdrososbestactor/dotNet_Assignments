﻿namespace LegacyCustomerDirectory.SearchResultsService.Enums
{
    /// <summary>
    /// OldCustomer Model. Contains all the information available for a Customer
    /// </summary>
    public enum PhoneNumberTypeId
    {
        Home = 2,
        Work = 3
    }
}

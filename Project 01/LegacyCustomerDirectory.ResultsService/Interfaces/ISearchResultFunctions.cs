﻿namespace LegacyCustomerDirectory.SearchResultsService.Interfaces
{
    public interface ISearchResultFunctions
    {
        string ConvertToJSON(object obj);
    }
}
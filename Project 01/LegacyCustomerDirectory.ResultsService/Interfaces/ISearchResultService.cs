﻿using LegacyCustomerDirectory.SearchResultsService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LegacyCustomerDirectory.SearchResultsService.Interfaces
{
    public interface ISearchResultService
    {
        Task<List<OldCustomerDto>> GetAllOldCustomersContainingTermAsync(DalConfig config, SearchTerms terms);
    }
}

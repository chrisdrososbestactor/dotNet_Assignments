﻿using LegacyCustomerDirectory.CustomersProvider;
using LegacyCustomerDirectory.CustomersProvider.Models;
using LegacyCustomerDirectory.DAL.EF;
using LegacyCustomerDirectory.SearchResultsService.Enums;
using LegacyCustomerDirectory.SearchResultsService.Interfaces;
using LegacyCustomerDirectory.SearchResultsService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalConfig = LegacyCustomerDirectory.SearchResultsService.Models.DalConfig;
using SearchTerms = LegacyCustomerDirectory.SearchResultsService.Models.SearchTerms;

namespace LegacyCustomerDirectory.SearchResultsService
{
    public class SearchResultService : ISearchResultService
    {
        public SearchResultService() { }
        /// <summary>
        /// Returns Task<list<OldCustomerDto>>> with any customer matching any term. BirthDate is being converted to Age
        /// Address should be of AddressTypeId==2 which is the Id for Home address
        /// WorkPhoneNumber should be of type PhoneNumberTypeId==3 and HomePhoneNumber of type PhoneNumberTypeId==2
        /// Also all Customers have maximum of one phone number in the database so there is no need to check for customers with 
        /// more than one phone number.
        /// </summary>
        /// <param name="config">DAL configuration</param>
        /// <param name="searchTerms">a List with string search terms</param>
        /// <returns></returns>
        public async Task<List<OldCustomerDto>> GetAllOldCustomersContainingTermAsync(DalConfig config, SearchTerms searchTerms)
        {
            var dalConfig = new DalConfig
            {
                ConnectionString = config.ConnectionString
            };
            var terms = new LegacyCustomerDirectory.CustomersProvider.Models.SearchTerms
            {
                AddressLine1 = searchTerms.AddressLine1,
                AddressLine2 = searchTerms.AddressLine2,
                City = searchTerms.City,
                EmailAddress = searchTerms.EmailAddress,
                FirstName = searchTerms.FirstName,
                Gender = searchTerms.Gender,
                JobTitle = searchTerms.JobTitle,
                LastName = searchTerms.LastName,
                PhoneNumber = searchTerms.PhoneNumber,
                PostalCode = searchTerms.PostalCode,
            };

            var contextFactory = new ContextFactory(config.ConnectionString);
            var tasks = new CustomerProvider(contextFactory);
            var results = await tasks.GetAllOldCustomersContainingTermAsync(terms);
            var dtoList = new List<OldCustomerDto>();
            IEnumerable<BusinessEntityPhones> phoneNumberLists = ExtractAllPhoneNumbersPerCustomer(results);

            foreach (OldCustomer result in results)
            {
                OldCustomerDto dto = ConvertToOldCustomerDto(result, phoneNumberLists
                    .Where(x => x.BusinessEntityId == result.BusinessEntityId).FirstOrDefault());
                dtoList.Add(dto);
            }
            return dtoList;
        }

        private IEnumerable<BusinessEntityPhones> ExtractAllPhoneNumbersPerCustomer(IEnumerable<OldCustomer> results)
        {
            return from r in results
                   group r by r.BusinessEntityId into grp
                   select new BusinessEntityPhones()
                   {
                       BusinessEntityId = grp.Key,
                       PhoneNumbers = grp.Select(x => new PhoneNumber()
                       {
                           PhoneNo = x.PhoneNumber,
                           PhoneType = x.PhoneNumberTypeId,
                       })
                   };
        }

        private OldCustomerDto ConvertToOldCustomerDto(OldCustomer result, BusinessEntityPhones businessEntityPhone)
        {
            var dto = new OldCustomerDto()
            {
                Age = CalculateAge(result.BirthDate),
                BusinessEntityId = result.BusinessEntityId,
                EmailAddress = result.EmailAddress,
                FirstName = result.FirstName,
                Gender = result.Gender,
                JobTitle = result.JobTitle,
                LastName = result.LastName,
                MaritalStatus = result.MaritalStatus,
            };
            dto.HomePhoneNumber = businessEntityPhone.PhoneNumbers
                .Where(p => p.PhoneType == (int)PhoneNumberTypeId.Home)
                .Select(p => p.PhoneNo)
                .FirstOrDefault();
            dto.WorkPhoneNumber = businessEntityPhone.PhoneNumbers
                 .Where(p => p.PhoneType == (int)PhoneNumberTypeId.Work)
                 .Select(p => p.PhoneNo)
                 .FirstOrDefault();

            // Checking for Address, only Home Address type is requested
            if (result.AddressTypeId == (int)AddressTypeId.Home)
            {
                dto.AddressLine1 = result.AddressLine1;
                dto.AddressLine2 = result.AddressLine2;
                dto.City = result.City;
                dto.PostalCode = result.PostalCode;
            }

            return dto;
        }

        private int CalculateAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            var age = DateTime.Now.Year - birthDate.Year;
            if (birthDate > today.AddYears(-age)) age--;
            return age;
        }
    }
}

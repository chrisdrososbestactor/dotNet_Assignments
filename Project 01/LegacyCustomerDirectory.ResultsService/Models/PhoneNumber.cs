﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LegacyCustomerDirectory.SearchResultsService.Models
{
	public class PhoneNumber
	{
		public string PhoneNo { get; set; }
		public int PhoneType { get; set; }
	}
}

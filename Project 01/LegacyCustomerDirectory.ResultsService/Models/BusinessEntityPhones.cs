﻿using System.Collections.Generic;

namespace LegacyCustomerDirectory.SearchResultsService.Models
{
    public class BusinessEntityPhones
    {
        public int BusinessEntityId { get; set; }
        public IEnumerable<PhoneNumber> PhoneNumbers { get; set; }
    }
}

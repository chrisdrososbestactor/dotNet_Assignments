﻿namespace LegacyCustomerDirectory.SearchResultsService.Models
{
    /// <summary>
    /// OldCustomer Model. Contains all the information available for a Customer
    /// </summary>
    public class OldCustomerDto
    {
        public int BusinessEntityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public int Age { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}

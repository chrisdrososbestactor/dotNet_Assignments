﻿using System;

namespace LegacyCustomerDirectory.SearchResultsService.Models
{
    public class DalConfig
    {
        // All the configuration parameter for the connection with the server goes here
        public string ConnectionString { get; set; } = String.Empty;

    }
}

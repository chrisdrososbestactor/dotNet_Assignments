﻿using LegacyCustomerDirectory.SearchResultsService.Interfaces;
using Newtonsoft.Json;

namespace LegacyCustomerDirectory.SearchResultsService
{
    public class SearchResultFunctions : ISearchResultFunctions
    {
        /// <summary>
        /// Converts the given objct to JSON
        /// </summary>
        /// <param name="obj">Any object</param>
        /// <returns></returns>
        public string ConvertToJSON(object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}

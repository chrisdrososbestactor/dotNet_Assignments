﻿using System;
using System.Collections.Generic;

namespace OldCustomers.Database
{
    public partial class SalesOrderHeaderSalesReasons
    {
        public int SalesOrderId { get; set; }
        public int SalesReasonId { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace OldCustomers.Database
{
    public partial class SalesReasons
    {
        public int SalesReasonId { get; set; }
        public string Name { get; set; }
        public string ReasonType { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}

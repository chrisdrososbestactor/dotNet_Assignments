﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RestaurantDelivery.BackOfficeMVC.Models;
using RestaurantDelivery.Service.Interfaces;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;


namespace RestaurantDelivery.BackOfficeMVC.Controllers
{
    public class CustomerController : Controller
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly ICustomerRepo CustomerRepo;

        public CustomerController(ILogger<CustomerController> logger, ICustomerRepo customerRepo)
        {
            CustomerRepo = customerRepo;
            _logger = logger;
        }

        public IActionResult AddCustomer()
        {
            return View();
        }

        public IActionResult SearchCustomer()
        {
            return View();
        }

        [HttpPost]
        [RequestFormSizeLimit(valueCountLimit: 20000)]
        public async Task<IActionResult> SearchResults(Customer cus)
        {
            var searchTerms = CreateSearchTermsFromCustomer(cus);
            var searchResults = await CustomerRepo.GetAllCustomersContainingTermAsync(searchTerms);
            var searchResultsFromAdventreWorks = await CustomerRepo
                .GetAllOldEmployeesWithTermsIgnoringMigratedIdsAsync(searchTerms,
                GetMigratedBusinessIdList(searchResults));

            var customersFounded = new CustomerSearchResultsViewModel()
            {
                ExistingCustomer = MapCustomerMVCModelFromService(searchResults.ToList()),
                AdvWorksCustomer = MapOldCustomerMVCModelFromService(searchResultsFromAdventreWorks.ToList()),
            };
            return View(customersFounded);
        }

        [HttpPost]
        [RequestFormSizeLimit(valueCountLimit: 20000)]
        public IActionResult MigrateOldCustomers(CustomerSearchResultsViewModel cusResults)
        {
            var results = new List<RestaurantDelivery.Service.Models.Customer>();
            results = MapServiceCustomerFromOldCustomer(cusResults.AdvWorksCustomer.Where(s => s.IsSelected == true).ToList());
            CustomerRepo.MigrateCustomers(results);
            var message = "Migration was successful.";
            return RedirectToAction("Index", "Home", new { message });
        }

        [HttpPost]
        public IActionResult AddCustomer(Customer cus)
        {
            if (ModelState.IsValid)
            {
                var newCustomer = new RestaurantDelivery.Service.Models.Customer
                {
                    AddressLine1 = cus.AddressLine1,
                    AddressLine2 = cus.AddressLine2,
                    Age = cus.Age,
                    City = cus.City,
                    EmailAddress = cus.EmailAddress,
                    FirstName = cus.FirstName,
                    Gender = cus.Gender,
                    HomePhoneNumber = cus.HomePhoneNumber,
                    JobTitle = cus.JobTitle,
                    LastName = cus.LastName,
                    MaritalStatus = cus.MaritalStatus,
                    BusinessEntityId = cus.BusinessEntityId,
                    PostalCode = cus.PostalCode,
                    WorkPhoneNumber = cus.WorkPhoneNumber,
                };
                CustomerRepo.AddNewCustomer(newCustomer);
                var message = "The User has been added successfully";
                return RedirectToAction("Index", "Home", new { message });
            }
            else
                return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private List<int> GetMigratedBusinessIdList(IEnumerable<Service.Models.Customer> searchResults)
        {
            return searchResults
                            .Where(x => x.BusinessEntityId != 0)
                            .Select(x => x.BusinessEntityId)
                            .ToList();
        }

        private List<SelectCustomerViewModel> MapOldCustomerMVCModelFromService(List<RestaurantDelivery.Service.Models.Customer> cus)
        {
            var result = new List<SelectCustomerViewModel>();
            foreach (var customer in cus)
            {
                result.Add(new SelectCustomerViewModel()
                {
                    BusinessEntityId = customer.BusinessEntityId,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    City = customer.City,
                    EmailAddress = customer.EmailAddress,
                    Age = customer.Age,
                    MaritalStatus = customer.MaritalStatus,
                    Gender = customer.Gender,
                    FirstName = customer.FirstName,
                    HomePhoneNumber = customer.HomePhoneNumber,
                    JobTitle = customer.JobTitle,
                    LastName = customer.LastName,
                    PostalCode = customer.PostalCode,
                    WorkPhoneNumber = customer.WorkPhoneNumber,
                    Id = customer.Id
                });
            }
            return result;
        }

        private List<RestaurantDelivery.Service.Models.Customer> MapServiceCustomerFromOldCustomer(List<RestaurantDelivery.BackOfficeMVC.Models.SelectCustomerViewModel> cus)
        {
            var result = new List<RestaurantDelivery.Service.Models.Customer>();
            foreach (var customer in cus)
            {
                result.Add(new RestaurantDelivery.Service.Models.Customer()
                {
                    BusinessEntityId = customer.BusinessEntityId,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    City = customer.City,
                    EmailAddress = customer.EmailAddress,
                    Age = customer.Age,
                    MaritalStatus = customer.MaritalStatus,
                    Gender = customer.Gender,
                    FirstName = customer.FirstName,
                    HomePhoneNumber = customer.HomePhoneNumber,
                    JobTitle = customer.JobTitle,
                    LastName = customer.LastName,
                    PostalCode = customer.PostalCode,
                    WorkPhoneNumber = customer.WorkPhoneNumber,
                    Id = customer.Id,
                });
            }
            return result;
        }
        private List<RestaurantDelivery.Service.Models.Customer> MapServiceCustomerFromMVCCustomer(List<RestaurantDelivery.BackOfficeMVC.Models.Customer> cus)
        {
            var result = new List<RestaurantDelivery.Service.Models.Customer>();
            foreach (var customer in cus)
            {
                result.Add(new RestaurantDelivery.Service.Models.Customer()
                {
                    BusinessEntityId = customer.BusinessEntityId,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    City = customer.City,
                    EmailAddress = customer.EmailAddress,
                    Age = customer.Age,
                    MaritalStatus = customer.MaritalStatus,
                    Gender = customer.Gender,
                    FirstName = customer.FirstName,
                    HomePhoneNumber = customer.HomePhoneNumber,
                    JobTitle = customer.JobTitle,
                    LastName = customer.LastName,
                    PostalCode = customer.PostalCode,
                    WorkPhoneNumber = customer.WorkPhoneNumber,
                    Id = customer.Id,
                });
            }
            return result;
        }
        private List<Customer> MapCustomerMVCModelFromService(List<RestaurantDelivery.Service.Models.Customer> cus)
        {
            var result = new List<Customer>();
            foreach (var customer in cus)
            {
                result.Add(new Customer()
                {
                    BusinessEntityId = customer.BusinessEntityId,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    City = customer.City,
                    Age = customer.Age,
                    MaritalStatus = customer.MaritalStatus,
                    EmailAddress = customer.EmailAddress,
                    Gender = customer.Gender,
                    FirstName = customer.FirstName,
                    HomePhoneNumber = customer.HomePhoneNumber,
                    JobTitle = customer.JobTitle,
                    LastName = customer.LastName,
                    PostalCode = customer.PostalCode,
                    WorkPhoneNumber = customer.WorkPhoneNumber,
                    Id = customer.Id,
                });
            }
            return result;
        }

        private RestaurantDelivery.Service.Models.SearchTerms CreateSearchTermsFromCustomer(Customer cus)
        {
            return new RestaurantDelivery.Service.Models.SearchTerms()
            {
                AddressLine1 = cus.AddressLine1,
                AddressLine2 = cus.AddressLine2,
                City = cus.City,
                EmailAddress = cus.EmailAddress,
                Gender = cus.Gender,
                FirstName = cus.FirstName,
                HomePhoneNumber = cus.HomePhoneNumber,
                JobTitle = cus.JobTitle,
                LastName = cus.LastName,
                PostalCode = cus.PostalCode,
                WorkPhoneNumber = cus.WorkPhoneNumber,
            };
        }


    }
}

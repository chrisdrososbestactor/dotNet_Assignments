﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RestaurantDelivery.BackOfficeMVC.Models;
using RestaurantDelivery.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RestaurantDelivery.BackOfficeMVC.Controllers
{
    public class ProductController : Controller
    {
        private readonly ILogger<ProductController> _logger;
        private readonly IProductRepo ProductRepo;

        public ProductController(ILogger<ProductController> logger, IProductRepo productRepo)
        {
            ProductRepo = productRepo;
            _logger = logger;
        }

        public async Task<IActionResult> AddNewProduct()
        {
            Models.CreateProductViewModel createProduct = await GenerateProductWithCategoriesModel();
            return View(createProduct);
        }

        private async Task<Models.CreateProductViewModel> GenerateProductWithCategoriesModel()
        {
            var categories = await ProductRepo.GetProductCategories();
            if (categories.Count == 0)
            {
                throw new Exception("There aren't any Product Categories, Please contact Database Administrator to add some categories!");
            }
            var createProduct = new Models.CreateProductViewModel();
            var productCategories = new List<Models.ProductCategory>();
            foreach (var category in categories)
            {
                productCategories.Add(new Models.ProductCategory
                {
                    Id = category.Id,
                    Description = category.Description,
                    Name = category.Name,
                });
            }
            createProduct.ProductCategories = productCategories;
            return createProduct;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewProduct(CreateProductViewModel prd)
        {
            if (ModelState.IsValid)
            {
                var newCreateProductItem = ConvertBackOfficeMVCToServiceModel(prd);
                ProductRepo.AddNewProduct(newCreateProductItem);
                var message = "The Product has been added successfully";
                return RedirectToAction("Index", "Home", new { message });
            }
            else
            {
                var createProduct = await GenerateProductWithCategoriesModel();
                return View(createProduct);
            }
        }

        private Service.Models.CreateProduct ConvertBackOfficeMVCToServiceModel(CreateProductViewModel prd)
        {
            return new RestaurantDelivery.Service.Models.CreateProduct
            {
                Description = prd.Description,
                Name = prd.Name,
                ProductPrice = prd.ProductPrice,
                CategoryId = prd.CategoryId
            };
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

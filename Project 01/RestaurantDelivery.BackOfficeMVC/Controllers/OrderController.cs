﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RestaurantDelivery.BackOfficeMVC.Models;
using RestaurantDelivery.Service.Interfaces;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace RestaurantDelivery.BackOfficeMVC.Controllers
{
    public class OrderController : Controller
    {
        private readonly ILogger<OrderController> _logger;
        private readonly IOrderRepo OrderRepo;
        private readonly IProductRepo ProductRepo;
        private readonly ICustomerRepo CustomerRepo;

        public OrderController(ILogger<OrderController> logger, IProductRepo productRepo,
            ICustomerRepo customerRepo, IOrderRepo orderRepo)
        {
            ProductRepo = productRepo;
            CustomerRepo = customerRepo;
            OrderRepo = orderRepo;
            _logger = logger;
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult SearchCustomer()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SelectProducts(List<SelectCustomerViewModel> selectedCustomer)
        {
            var products = await ProductRepo.GetProducts();
            if (products.Count == 0)
            {
                var message = "No products are available. Create some products first.";
                return RedirectToAction("Index", "Home", new { message });
            }
            var cart = new OrderCartViewModel();
            cart.CustomerId = selectedCustomer.Where(x => x.IsSelected == true).FirstOrDefault().Id;
            cart.Products = ConvertToSelectProductViewModel(products.OrderBy(x => x.ProductCategory).ToList());

            return View(cart);
        }

        [HttpPost]
        public IActionResult CreateOrder(OrderCartViewModel cart)
        {
            var order = new RestaurantDelivery.Service.Models.Order();
            var customer = new Service.Models.Customer();
            customer.Id = cart.CustomerId;
            order.Customer = customer;
            var product = new Service.Models.Product();
            var selectedProduct = cart.Products.Where(x => x.IsSelected == true).FirstOrDefault();
            product.Id = selectedProduct.Id;
            order.Product = product;
            order.OrderPrice = selectedProduct.ProductPrice;
            OrderRepo.CreateOrder(order);

            var message = "Order was registered successfully</br>It will be created Asynchronously!.</br>Don't Forget to tell the client to await for it!!!";
            return RedirectToAction("Index", "Home", new { message });
        }

        private List<SelectProductViewModel> ConvertToSelectProductViewModel(List<RestaurantDelivery.Service.Models.Product> p)
        {
            var result = new List<SelectProductViewModel>();
            foreach (var prod in p)
            {


                result.Add(new SelectProductViewModel { 
                    Description = prod.Description,
                    Id =prod.Id,
                    Name = prod.Name,
                    //ProductCategory = prod.ProductCategory,
                    ProductPrice = prod.ProductPrice,
                    
                });
            }
            return result;

        }

        private RestaurantDelivery.BackOfficeMVC.Models.Product ConvertToBackOfficeMVCProduct(RestaurantDelivery.Service.Models.Product p)
        {
            return new RestaurantDelivery.BackOfficeMVC.Models.Product
            {
                Description = p.Description,
                Id = p.Id,
                Name = p.Name,
                ProductCategory = ConvertToBackOfficeMVCProductCategory(p.ProductCategory),
                ProductPrice = p.ProductPrice,
                Quantity = p.Quantity,
            };

        }

        private RestaurantDelivery.BackOfficeMVC.Models.ProductCategory ConvertToBackOfficeMVCProductCategory(RestaurantDelivery.Service.Models.ProductCategory p)
        {
            return new RestaurantDelivery.BackOfficeMVC.Models.ProductCategory
            {
                Description = p.Description,
                Id = p.Id,
                Name = p.Name,

            };
        }

        private List<RestaurantDelivery.BackOfficeMVC.Models.Product> MapToBackOfficeMVCModel(List<RestaurantDelivery.Service.Models.Product> p)
        {
            var result = new List<RestaurantDelivery.BackOfficeMVC.Models.Product>();
            foreach (var product in p)
            {
                result.Add(new RestaurantDelivery.BackOfficeMVC.Models.Product
                {

                });

            }
            return result;
        }

        [HttpPost]
        public async Task<IActionResult> SelectCustomer(Customer cus)
        {
            var searchTerms = CreateSearchTermsFromCustomer(cus);
            var searchResults = await CustomerRepo.GetAllCustomersContainingTermAsync(searchTerms);
            return View(MapSelectCustomerViewModelFromService(searchResults.ToList()));
        }
        private RestaurantDelivery.Service.Models.SearchTerms CreateSearchTermsFromCustomer(Customer cus)
        {
            return new RestaurantDelivery.Service.Models.SearchTerms()
            {

                AddressLine1 = cus.AddressLine1,
                AddressLine2 = cus.AddressLine2,
                City = cus.City,
                EmailAddress = cus.EmailAddress,
                Gender = cus.Gender,
                FirstName = cus.FirstName,
                HomePhoneNumber = cus.HomePhoneNumber,
                JobTitle = cus.JobTitle,
                LastName = cus.LastName,
                PostalCode = cus.PostalCode,
                WorkPhoneNumber = cus.WorkPhoneNumber,
            };
        }

        private List<SelectCustomerViewModel> MapSelectCustomerViewModelFromService(List<RestaurantDelivery.Service.Models.Customer> cus)
        {
            var result = new List<SelectCustomerViewModel>();
            foreach (var customer in cus)
            {
                result.Add(new SelectCustomerViewModel()
                {
                    Id = customer.Id,
                    BusinessEntityId = customer.BusinessEntityId,
                    AddressLine1 = customer.AddressLine1,
                    AddressLine2 = customer.AddressLine2,
                    City = customer.City,
                    EmailAddress = customer.EmailAddress,
                    Age = customer.Age,
                    MaritalStatus = customer.MaritalStatus,
                    Gender = customer.Gender,
                    FirstName = customer.FirstName,
                    HomePhoneNumber = customer.HomePhoneNumber,
                    JobTitle = customer.JobTitle,
                    LastName = customer.LastName,
                    PostalCode = customer.PostalCode,
                    WorkPhoneNumber = customer.WorkPhoneNumber,
                });
            }
            return result;
        }
    }
}

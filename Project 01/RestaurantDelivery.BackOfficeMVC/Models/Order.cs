﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class Order
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public DateTime DateOrdered { get; set; }
        public int OrderPrice { get; set; }
    }
}

﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class Product
    {
        [BindNever]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required]
        [Range(1, 100)]
        public int ProductPrice { get; set; }
        public ProductCategory ProductCategory { get; set; }
        public int Quantity { get; set; }
    }
}

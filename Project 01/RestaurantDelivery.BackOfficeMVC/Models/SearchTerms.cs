﻿namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// A list of strings with the terms that will be used to search    
    public class SearchTerms
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Gender { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string JobTitle { get; set; }
        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        public string PostalCode { get; set; }

    }
}

﻿using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class ProductCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }


    }
}

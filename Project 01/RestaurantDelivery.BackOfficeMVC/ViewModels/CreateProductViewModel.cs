﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class CreateProductViewModel : Product
    {
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category Required")]
        public int CategoryId { get; set; }
        [Display(Name = "Category")]
        [Required(ErrorMessage = "Category Required")]
        public List<ProductCategory> ProductCategories { get; set; }
    }
}

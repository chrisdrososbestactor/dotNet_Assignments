﻿namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Customer Model. Contains all the information available for a Customer
    /// </summary>
    public class SelectCustomerViewModel : Customer
    {
        public bool IsSelected { get; set; }
    }
}

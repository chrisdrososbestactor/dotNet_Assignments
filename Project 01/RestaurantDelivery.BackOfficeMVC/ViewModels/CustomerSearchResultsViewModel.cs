﻿using System.Collections.Generic;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Customer Model. Contains all the information available for a Customer
    /// </summary>
    public class CustomerSearchResultsViewModel
    {
        public List<Customer> ExistingCustomer { get; set; }
        public List<SelectCustomerViewModel> AdvWorksCustomer { get; set; }
    }
}

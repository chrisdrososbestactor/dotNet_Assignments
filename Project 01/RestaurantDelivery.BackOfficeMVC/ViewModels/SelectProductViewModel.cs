﻿namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Order View Model. Contains a boolean to get the selected Products
    /// </summary>
    public class SelectProductViewModel : Product
    {
        public bool IsSelected { get; set; }
    }
}

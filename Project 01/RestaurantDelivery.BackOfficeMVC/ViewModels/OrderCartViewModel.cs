﻿using System.Collections.Generic;

namespace RestaurantDelivery.BackOfficeMVC.Models
{
    /// <summary>
    /// Customer Model. Contains all the information available for a Customer
    /// </summary>
    public class OrderCartViewModel
    {
        public int CustomerId { get; set; }
        public List<SelectProductViewModel> Products { get; set; }
    }
}

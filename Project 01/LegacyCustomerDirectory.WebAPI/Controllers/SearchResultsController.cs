﻿using LegacyCustomerDirectory.SearchResultsService.Interfaces;
using LegacyCustomerDirectory.SearchResultsService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace LegacyCustomerDirectoryWebAPI.Controllers
{
    /// <summary>
    /// Generates results with customer information. 
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class SearchResultsController : ControllerBase
    {
        readonly ILogger<SearchResultsController> _logger;
        ISearchResultService ResultService;
        ISearchResultFunctions ResultFunctions;
        DalConfig DalConfig;
        public SearchResultsController(ILogger<SearchResultsController> logger
            , ISearchResultService resultService, ISearchResultFunctions resultFunctions, IOptions<DalConfig> settings)
        {
            DalConfig = settings.Value;
            ResultService = resultService;
            ResultFunctions = resultFunctions;
            _logger = logger;
        }
        /// <summary>
        /// Returns all customers with values that matches in any of the terms
        /// Searches all the customer fields except BusinessEntityId and BirthDate
        /// </summary>
        /// <param name="terms">a list of string terms, seperated by space</param>
        /// <returns></returns>
        [HttpGet("FindOldCustomers")]
        public async Task<IActionResult> GetAllOldCustomersContainingTermAsync()
        {
            var terms = new SearchTerms();
            if (Request != null)
            {
                terms.FirstName = Request.Query["FirstName"];
                terms.LastName = Request.Query["LastName"];
                terms.EmailAddress = Request.Query["EmailAddress"];
                terms.Gender = Request.Query["Gender"];
                terms.AddressLine1 = Request.Query["AddressLine1"];
                terms.AddressLine2 = Request.Query["AddressLine2"];
                terms.City = Request.Query["City"];
                terms.JobTitle = Request.Query["JobTitle"];
                terms.PhoneNumber = Request.Query["PhoneNumber"];
                terms.PostalCode = Request.Query["PostalCode"];
            }

            var config = new DalConfig()
            {
                ConnectionString = DalConfig.ConnectionString,
            };
            var task = await ResultService.GetAllOldCustomersContainingTermAsync(config, terms);
            if (task.Count == 0)
                return NotFound();
            return Ok(ResultFunctions.ConvertToJSON(task));
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RestaurantDelivery.DAL.EF;
using RestaurantDelivery.Service.Interfaces;
using RestaurantDelivery.Service.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
//using Customer = RestaurantDelivery.DAL.EF.Models.Customer;

namespace RestaurantDelivery.Service
{
    /// <summary>
    /// Results provider for EFCore, Using DI with AddDbContextPool
    /// </summary>
    public class CustomerRepo : ICustomerRepo
    {
        RestaurantDeliveryContext CurrentContext;
        readonly DalConfig Config;
        readonly string BaseUrl;
        public CustomerRepo(IOptions<DalConfig> config)
        {
            Config = config.Value;
            BaseUrl = Config.AdventurwWorksURL;
        }
        public RestaurantDeliveryContext GetContext()
        {
            if (CurrentContext != null)
                return CurrentContext;
            else
                return CurrentContext = new RestaurantDelivery.DAL.EF.ContextFactory(Config.ConnectionString).GetContext();
        }
        public async Task<List<Customer>> GetAllOldEmployeesWithTermsIgnoringMigratedIdsAsync(SearchTerms terms,
            List<int> MigratedIds, string subUrl = "/SearchResults/FindOldCustomers/")
        {
            var queryString = ConvertSearchTermsToQueryString(terms);
            var resultData = new List<Customer>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl + queryString);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    resultData = JsonConvert.DeserializeObject<List<Customer>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            return resultData.Where(r => !MigratedIds.Any(p2 => p2 == r.BusinessEntityId)).ToList();
        }

        public void MigrateCustomers(List<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                GetContext().Customer.Add(MapServiceToDALEFModel(customer));
            }
            GetContext().SaveChanges();
        }

        public async Task MigrateCustomerById(List<int> businessEntityId,
            string subUrl = "/SearchResults/FindOldCustomers/?BusinessEntityId=")
        {
            var newCustomers = new List<Customer>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri(BaseUrl);
                var responsTask = await client.GetAsync(subUrl + businessEntityId);
                if (responsTask.IsSuccessStatusCode)
                {
                    var readTask = await responsTask.Content.ReadAsStringAsync();
                    newCustomers = JsonConvert.DeserializeObject<List<Customer>>(readTask);
                }
                else
                    throw new System.FormatException("The format of the variable which represent the selected DAL was not correct");
            }
            foreach (Customer customer in newCustomers)
            {
                GetContext().Customer.Add(MapServiceToDALEFModel(customer));
            }
            GetContext().SaveChanges();
        }

        private RestaurantDelivery.DAL.EF.Models.Customer MapServiceToDALEFModel(Customer c)
        {
            return new RestaurantDelivery.DAL.EF.Models.Customer
            {
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                Age = c.Age,
                BusinessEntityId = c.BusinessEntityId,
                City = c.City,
                EmailAddress = c.EmailAddress,
                FirstName = c.FirstName,
                Gender = c.Gender,
                JobTitle = c.JobTitle,
                LastName = c.LastName,
                MaritalStatus = c.MaritalStatus,
                HomePhoneNumber = c.HomePhoneNumber,
                WorkPhoneNumber = c.WorkPhoneNumber,
                PostalCode = c.PostalCode,
                Id = c.Id,
            };
        }
        /// <summary>
        /// Returns all the Customers that matches any string field with any of the terms
        /// Only Home address, Work phone number and Home phone number are requested.
        /// </summary>
        /// <param name="terms">Array of search terms</param>
        /// <returns>List of Customers</returns>
        public async Task<IEnumerable<Customer>> GetAllCustomersContainingTermAsync(SearchTerms terms)
        {
            var query = GetContext().Customer.Select(c => new Customer
            {
                Id = c.Id,
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                Age = c.Age,
                BusinessEntityId = c.BusinessEntityId,
                City = c.City,
                EmailAddress = c.EmailAddress,
                FirstName = c.FirstName,
                Gender = c.Gender,
                JobTitle = c.JobTitle,
                LastName = c.LastName,
                MaritalStatus = c.MaritalStatus,
                HomePhoneNumber = c.HomePhoneNumber,
                WorkPhoneNumber = c.WorkPhoneNumber,
                PostalCode = c.PostalCode,
            });
            query = SearchForTerms(query, terms);
            return await query.ToListAsync();
        }
        public async Task<IEnumerable<Customer>> GetAllOldCustomersContainingTermAsync(SearchTerms terms)
        {
            var query = GetContext().Customer.Select(c => new Customer
            {
                Id = c.Id,
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                Age = c.Age,
                BusinessEntityId = c.BusinessEntityId,
                City = c.City,
                EmailAddress = c.EmailAddress,
                FirstName = c.FirstName,
                Gender = c.Gender,
                JobTitle = c.JobTitle,
                LastName = c.LastName,
                MaritalStatus = c.MaritalStatus,
                HomePhoneNumber = c.HomePhoneNumber,
                WorkPhoneNumber = c.WorkPhoneNumber,
                PostalCode = c.PostalCode,
            });
            query = SearchForTerms(query, terms);
            return await query.ToListAsync();
        }
        /// <summary>
        /// Searches for every Customer that match any of the available term.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="terms"></param>
        /// <returns></returns>
        private IQueryable<Customer> SearchForTerms(IQueryable<Customer> query, SearchTerms terms)
        {
            if (!string.IsNullOrEmpty(terms.FirstName))
                query = query.Where(x => x.FirstName.Contains(terms.FirstName));
            if (!string.IsNullOrEmpty(terms.LastName))
                query = query.Where(x => x.LastName.Contains(terms.LastName));
            if (!string.IsNullOrEmpty(terms.Gender))
                query = query.Where(x => x.Gender.Contains(terms.Gender));
            if (!string.IsNullOrEmpty(terms.EmailAddress))
                query = query.Where(x => x.EmailAddress.Contains(terms.EmailAddress));
            if (!string.IsNullOrEmpty(terms.City))
                query = query.Where(x => x.City.Contains(terms.City));
            if (!string.IsNullOrEmpty(terms.JobTitle))
                query = query.Where(x => x.JobTitle.Contains(terms.JobTitle));
            if (!string.IsNullOrEmpty(terms.HomePhoneNumber))
                query = query.Where(x => x.HomePhoneNumber.Contains(terms.HomePhoneNumber));
            if (!string.IsNullOrEmpty(terms.WorkPhoneNumber))
                query = query.Where(x => x.HomePhoneNumber.Contains(terms.WorkPhoneNumber));
            if (!string.IsNullOrEmpty(terms.AddressLine1))
                query = query.Where(x => x.AddressLine1.Contains(terms.AddressLine1));
            if (!string.IsNullOrEmpty(terms.AddressLine2))
                query = query.Where(x => x.AddressLine2.Contains(terms.AddressLine2));
            if (!string.IsNullOrEmpty(terms.PostalCode))
                query = query.Where(x => x.PostalCode.Contains(terms.PostalCode));
            return query;
        }

        public void AddNewCustomer(Customer cust)
        {
            var newCustomer = new RestaurantDelivery.DAL.EF.Models.Customer()
            {
                Id = cust.Id,
                AddressLine1 = cust.AddressLine1,
                AddressLine2 = cust.AddressLine2,
                Age = cust.Age,
                City = cust.City,
                EmailAddress = cust.EmailAddress,
                FirstName = cust.FirstName,
                Gender = cust.Gender,
                HomePhoneNumber = cust.HomePhoneNumber,
                JobTitle = cust.JobTitle,
                LastName = cust.LastName,
                MaritalStatus = cust.MaritalStatus,
                BusinessEntityId = cust.BusinessEntityId,
                PostalCode = cust.PostalCode,
                WorkPhoneNumber = cust.WorkPhoneNumber,
            };
            GetContext().Customer.Add(newCustomer);
            GetContext().SaveChanges();
        }

        public string ConvertSearchTermsToQueryString(SearchTerms terms)
        {
            var queryString = "?";
            foreach (var t in terms.GetType().GetProperties()
                .Where(t => !t.GetGetMethod().GetParameters().Any()))
            {
                if (t.GetValue(terms, null) != null)
                    queryString += t.Name + "=" + t.GetValue(terms, null) + "&";
            }
            return queryString.Remove(queryString.Length - 1);
        }
    }
}
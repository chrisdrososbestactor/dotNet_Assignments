﻿using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.Service.Models
{
    /// <summary>
    /// Customer Model. Contains all the information available for a Customer
    /// </summary>
    public class Customer
    {
        public int Id { get; set; }
        public int BusinessEntityId { get; set; }
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public int Age { get; set; }
        public string MaritalStatus { get; set; }
        public string Gender { get; set; }
        public string EmailAddress { get; set; }
        public string HomePhoneNumber { get; set; }
        public string WorkPhoneNumber { get; set; }
        [Required]
        [StringLength(100)]
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        [Required]
        [StringLength(50)]
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.Service.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class Order
    {
        [Key]
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public DateTime DateOrdered { get; set; }
        public int OrderPrice { get; set; }
    }
}

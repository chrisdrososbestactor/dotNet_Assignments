﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.Service.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class CreateProduct : Product
    {
        [Required]
        public int CategoryId { get; set; }
        public List<ProductCategory> ProductCategories { get; set; }
    }
}

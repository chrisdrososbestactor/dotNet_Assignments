﻿using System.ComponentModel.DataAnnotations;

namespace RestaurantDelivery.Service.Models
{
    /// <summary>
    /// Order Model. Contains all the information for an Order
    /// </summary>
    public class ProductCategory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}

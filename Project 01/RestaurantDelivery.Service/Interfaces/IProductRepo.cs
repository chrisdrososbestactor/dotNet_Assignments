﻿using RestaurantDelivery.DAL.EF;
using RestaurantDelivery.Service.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using ProductCategory = RestaurantDelivery.DAL.EF.Models.ProductCategory;

namespace RestaurantDelivery.Service.Interfaces
{
    public interface IProductRepo
    {
        RestaurantDeliveryContext GetContext();
        Task<List<RestaurantDelivery.Service.Models.Product>> GetProducts();
        Task<List<ProductCategory>> GetProductCategories();
        void AddNewProduct(CreateProduct prd);
    }
}
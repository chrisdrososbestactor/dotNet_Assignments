﻿using RestaurantDelivery.Service.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace RestaurantDelivery.Service.Interfaces
{
    public interface ICustomerRepo
    {
        Task<IEnumerable<Customer>> GetAllCustomersContainingTermAsync(SearchTerms terms);
        Task<IEnumerable<Customer>> GetAllOldCustomersContainingTermAsync(SearchTerms terms);
        Task<List<Customer>> GetAllOldEmployeesWithTermsIgnoringMigratedIdsAsync(SearchTerms terms,
            List<int> MigratedIds, string subUrl = "/SearchResults/FindOldCustomers/");
        Task MigrateCustomerById(List<int> businessEntityId,
            string subUrl = "/SearchResults/FindOldCustomers/?BusinessEntityId=");
        void AddNewCustomer(Customer customer);
        void MigrateCustomers(List<RestaurantDelivery.Service.Models.Customer> results);
    }
}
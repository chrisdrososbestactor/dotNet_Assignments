﻿using RestaurantDelivery.Service.Models;

namespace RestaurantDelivery.Service.Interfaces
{
    public interface IOrderRepo
    {
        void CreateOrder(Order order);
    }
}
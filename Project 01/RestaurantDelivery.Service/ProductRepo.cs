﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using RestaurantDelivery.DAL.EF;
using RestaurantDelivery.Service.Interfaces;
using RestaurantDelivery.Service.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Product = RestaurantDelivery.DAL.EF.Models.Product;
using ProductCategory = RestaurantDelivery.DAL.EF.Models.ProductCategory;

namespace RestaurantDelivery.Service
{
    /// <summary>
    /// Results provider for EFCore, Using DI with AddDbContextPool
    /// </summary>
    public class ProductRepo : IProductRepo
    {
        RestaurantDeliveryContext CurrentContext;
        readonly DalConfig Config;
        readonly string BaseURL;
        public ProductRepo(IOptions<DalConfig> config)
        {
            Config = config.Value;
            BaseURL = Config.AdventurwWorksURL;
        }
        public RestaurantDeliveryContext GetContext()
        {
            if (CurrentContext != null)
                return CurrentContext;
            else
                return CurrentContext = new RestaurantDelivery.DAL.EF.ContextFactory(Config.ConnectionString).GetContext();
        }

        public async Task<List<ProductCategory>> GetProductCategories()
        {
            var result = new List<ProductCategory>();
            using (var context = GetContext())
            {
                result = await context.ProductCategory.ToListAsync();
            }
            return result;
        }
        public async Task<List<RestaurantDelivery.Service.Models.Product>> GetProducts()
        {
            var result = new List<Product>();
            using (var context = GetContext())
            {
                result = await context.Product.Include(p => p.ProductCategory).ToListAsync();
            }
            return MapToServiceProducts(result);
        }

        private List<RestaurantDelivery.Service.Models.Product> MapToServiceProducts(List<Product> p)
        {
            var result = new List<RestaurantDelivery.Service.Models.Product>();
            foreach (var prod in p)
            {
                result.Add(new RestaurantDelivery.Service.Models.Product
                {
                    Description = prod.Description,
                    Id = prod.Id,
                    Name = prod.Name,
                    ProductCategory = MapToServiceCategory(prod.ProductCategory),
                    ProductPrice = prod.ProductPrice

                });
            }
            return result;
        }

        private RestaurantDelivery.Service.Models.ProductCategory MapToServiceCategory(ProductCategory pc)
        {
            if (pc == null)
                return null;
            var result = new RestaurantDelivery.Service.Models.ProductCategory
            {
                Description = pc.Description,
                Name = pc.Name,
                Id = pc.Id
            };
            return result;
        }

        public void AddNewProduct(CreateProduct prd)
        {
            var product = new Product()
            {
                Name = prd.Name,
                ProductPrice = prd.ProductPrice,
                Description = prd.Description,
                ProductCategory = GetContext().ProductCategory.First(c => c.Id == prd.CategoryId)
            };
            GetContext().Product.Add(product);
            GetContext().SaveChanges();
        }
    }
}
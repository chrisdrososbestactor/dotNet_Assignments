﻿using Microsoft.Extensions.Options;
using RestaurantDelivery.DAL.EF;
using RestaurantDelivery.Service.Interfaces;
using RestaurantDelivery.Service.Models;
using Order = RestaurantDelivery.Service.Models.Order;

namespace RestaurantDelivery.Service
{
    /// <summary>
    /// Results provider for EFCore, Using DI with AddDbContextPool
    /// </summary>
    public class OrderRepo : IOrderRepo
    {
        RestaurantDeliveryContext CurrentContext;
        readonly DalConfig Config;


        readonly string BaseUrl;
        public OrderRepo(IOptions<DalConfig> config)
        {
            Config = config.Value;
            BaseUrl = Config.AdventurwWorksURL;
        }
        public RestaurantDeliveryContext GetContext()
        {
            if (CurrentContext != null)
                return CurrentContext;
            else
                return CurrentContext = new RestaurantDelivery.DAL.EF.ContextFactory(Config.ConnectionString).GetContext();
        }

        public void CreateOrder(Order order)
        {
            //var config = new MapperConfiguration(cfg => cfg
            //.CreateMap<Order, RestaurantDelivery.DAL.EF.Models.Order>());
            //config = new MapperConfiguration(cfg => cfg.CreateMap<Product, RestaurantDelivery.DAL.EF.Models.Product>());
            // config = new MapperConfiguration(cfg => cfg.CreateMap<Customer, RestaurantDelivery.DAL.EF.Models.Customer>());
            //GetContext().Order.Add(order);
            GetContext().SaveChanges();
        }


        private RestaurantDelivery.DAL.EF.Models.Product MapToDALEF(Product product)
        {
            return new RestaurantDelivery.DAL.EF.Models.Product
            {
                Description = product.Description,
                Id = product.Id,
                Name = product.Name,
                //ProductCategory = product.ProductCategory,
                ProductPrice = product.ProductPrice,
            };
        }

        private RestaurantDelivery.DAL.EF.Models.Customer MapToDALEF(Customer customer)
        {
            return new RestaurantDelivery.DAL.EF.Models.Customer
            {

            };
        }

    }
}